# VOXL Documentation via GitLab Pages

If you are looking for the 'actual' VOXL documentation, please go here:

https://docs.modalai.com

---

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->


- [Summary](#summary)
- [Normal Usage](#normal-usage)
  - [Markdown](#markdown)
  - [Content Structure](#content-structure)
  - [just-the-docs Syntax](#just-the-docs-syntax)
  - [Publishing New Content](#publishing-new-content)
- [Testing Locally Using Docker](#testing-locally-using-docker)
- [GitLab CI/CD](#gitlab-cicd)
  - [Firebase](#firebase)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Summary

This project takes Markdown documentation in the `docs` directory and creates a user friendly website hosted at docs.modalai.com.

The project is using the [Jekyll](https://jekyllrb.com) static site generator which is just what Github uses when presenting Markdown to the user.  The [just-the-docs](https://pmarsceill.github.io/just-the-docs/) template is being used for a good user experience.

To run and test locally, a Docker Compose project is available and can be simply hosted running `docker-compose up` and accessing http://localhost:4000.

## Normal Usage

### Markdown

Content is created in Markdown.  There's some 'syntantic sugar' offered by [just-the-docs](https://pmarsceill.github.io/just-the-docs/) which can be referenced, and a few items are called out below.

### Content Structure

You'll do the content editing in here.  Creating a new directory with with name `cool-topic`, you'd create a `cool-topic.md` in the root of the new folder.
```
.
|
├── docs
|   ├── datasheets
|   |   ├── datasheets.md  (parent)
|   |   └── ...md          (children)
|   └── quickstarts
|   |   ├── quickstarts.md (parent)
|   |   └── ...md          (children)
|   └── user-guides
|   |   ├── user-guides.md (parent)
|   |   └── ...md          (children)
└── index.md
```

### just-the-docs Syntax

The `.md` file must have a header like this to be generated (if the file doens't have at least this header, it will not be visibile to the end user)

```
---
layout: default
---
```

If it's a parent page with children, use the following, where the `nav_order` specifies where in the side navigation bar the item appears:

```
---
layout: default
title: Some title
nav_order: N
has_children: true
---
```

A child page can be defined with a header like so:

```
---
layout: default
title: Another title
parent: Some title
nav_order: N
---
```

If you need to inline some HTML that you don't want to be processed, for example, a `<br>`:

```bash
{::nomarkdown} <br> {:/}
```

### Publishing New Content

Simpy push changes to the repo and the site will be deployed to docs.modalai.com

## Testing Locally Using Docker

You must have [Docker](https://docs.docker.com/install) and [Docker Compose](https://docs.docker.com/compose/install) installed.

You can run the project locally using docker with the following command from the root of this project:

```bash
docker-compose up
```

Now, access the website at http://localhost:4000

To stop the container, use `CTRL+C`

## GitLab CI/CD

### Firebase

The GitLab CI will depoly to to Firebase using an environment variable in the CI script.  This token is initially generated on the host system by:

- Logging in to firebase through the CLI
- running the `firebase login:ci`

Then, in GitLab under `Settings > CI/CD`, go to `Variables`, and setup a `FIREBASE_TOKEN` variable with the value.
