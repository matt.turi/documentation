---
layout: default
title: VOXL m500 Functional Description
parent: VOXL m500 Reference Drone Datasheet
grand_parent: Datasheets
nav_order: 1
permalink: /voxl-m500-functional-description/
---

# VOXL m500 Snapdragon Reference Drone Functional Description
{: .no_toc }

[User Guide Here](/voxl-m500-getting-started/)

[Buy Here](https://www.modalai.com/m500)

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![voxl-m500](../images/datasheet/m500/m500-shopify.jpg)

## High-Level Specs

| Specicifcation | [VOXL-M500-DK-R2-2](https://www.modalai.com/collections/development-drones/products/voxl-m500?variant=31790290599987) | [VOXL-M500-DK-R2-G1-2](https://www.modalai.com/collections/development-drones/products/voxl-m500?variant=31793533845555) |
| --- | --- | --- |
| Take-off Weight | 1075g with battery (820g w/o battery) | TBD |
| Size |  Length: 15.5" <br> Width: 15.5" <br> Height to: <br> - Bottom Plate: 2.25" <br> - Top of Flight Deck: 4.5" <br> - Top of GPS: 8" | Length: 15.5" <br> Width: 15.5" <br> Height to: <br> - Bottom Plate: 6.25" <br> - Top of Flight Deck: 8.5" <br> - Top of GPS: 12"  |
| Flight Time | >20 minutes | TBD |
| Payload Capacity (Impacts Flight Time) | 1kg | TBD |
| Motors | [2216 880KV](https://shop.holybro.com/motor2216-880kv-1pc_p1154.html) | [2216 880KV](https://shop.holybro.com/motor2216-880kv-1pc_p1154.html) |
| Propellers | 10" (1045) <br>[Replacement](https://shop.holybro.com/s500-v2-propeller10452pair_p1155.html) | 10" (1045) <br>[Replacement](https://shop.holybro.com/s500-v2-propeller10452pair_p1155.html) |
| Frame | s500 <br>[Replacement](https://shop.holybro.com/s500-v2-frame-kit_p1139.html) | s500 <br>[Replacement](https://shop.holybro.com/s500-v2-frame-kit_p1139.html) |
| ESCs* | [BLHeli 20A](https://shop.holybro.com/blheli-esc-20a_p1143.html) | [BLHeli 20A](https://shop.holybro.com/blheli-esc-20a_p1143.html) |
| Computing | [VOXL Flight](/voxl-flight-datasheet/) | [VOXL Flight](/voxl-flight-datasheet/) |
| Flight Control | PX4 on [VOXL Flight](/voxl-flight-datasheet/) | PX4 on [VOXL Flight](/voxl-flight-datasheet/) |
| Image Sensors | [Stereo](/voxl-stereo-camera-datasheet/), [Tracking](/voxl-tracking-camera-datasheet/), [4k High-resolution](/voxl-hires-camera-datasheet/) |  [Stereo](/voxl-stereo-camera-datasheet/), [Tracking](/voxl-tracking-camera-datasheet/), [4k High-resolution](/voxl-hires-camera-datasheet/) |
| Communication | WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional |  WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional |
| Remote Control | Configured for spektrum, compatible with most DIY R/C systems through expansion I/O on [Flight Core](/flight-core-datasheet/) |  Configured for spektrum, compatible with most DIY R/C systems through expansion I/O on [Flight Core](/flight-core-datasheet/) |
| Power Module | [Power Module v3](power-module-v3-datasheet) |  [Power Module v3](power-module-v3-datasheet) |
| Battery | Up to 5S (not included, [recommended - select XT60 connector](https://www.thunderpowerrc.com/products/tp3400-4spx25?variant=31080855339072)) | Up to 5S (not included, [recommended - select XT60 connector](https://www.thunderpowerrc.com/products/tp3400-4spx25?variant=31080855339072)) |
| Additional Components | N/A | Gimbal w/o Camera, details below |
| Landing Gear | [Polycarbonate Round Tube 1/2" OD, 3/8" ID, Clear](https://www.mcmaster.com/2044T42/) | [Polycarbonate Round Tube 1/2" OD, 3/8" ID, Clear](https://www.mcmaster.com/2044T42/) |
| Battery Standoffs | Screws: Black-Oxide Alloy Steel Socket Head Screw, M2.5 x 0.45 mm Thread, 20 mm Long [91290A108](https://www.mcmaster.com/catalog/126/3290/) <br> Spacers: Aluminum Unthreaded Spacer, 4.500 mm OD, 14 mm Long, for M2.5 Screw Size [94669A108](https://www.mcmaster.com/catalog/126/3464) | Screws: Black-Oxide Alloy Steel Socket Head Screw, M2.5 x 0.45 mm Thread, 20 mm Long [91290A108](https://www.mcmaster.com/catalog/126/3290/) <br> Spacers: Aluminum Unthreaded Spacer, 4.500 mm OD, 14 mm Long, for M2.5 Screw Size [94669A108](https://www.mcmaster.com/catalog/126/3464) |

* NOTE: very early m500 shipments have ESCs with "black/red/white" wires, where latter shipments have "black/red/yellow" wires.  These ESCs have slightly different performance and shouldn't be mixed together on the same vehicle.

## System Overview

[View in fullsize](/images/datasheet/m500/VOXL-m500-R2-architecture.png){:target="_blank"}
![m500-.png](/images/datasheet/m500/VOXL-m500-R2-architecture.png)

## m500 Labeled

[View in fullsize](/images/datasheet/m500/labeled_m500.png){:target="_blank"}
![m500-labeled.png](/images/datasheet/m500/labeled_m500.png)

## PX4 Tuning Parameters

Our engineers (namely James!) have spent a lot of time finely tuning this vehicle, and the [parameters are available here](https://gitlab.com/voxl-public/px4-parameters)

## Connectivity for Remote Operation

| Connectivity Option | Use Case | Details |
| --- | --- | --- |
| Spektrum R/C | Manual remote control of the vehicle | [Configure](https://docs.modalai.com/configure-rc-radio/) |
| WiFi | Short range (~100m) IP connectivity for debug and nearby flights | [Setup](https://docs.modalai.com/wifi-setup/) |
| LTE | Long Range, BVLOS operation | [User Manual](/lte-v2-modem-manual/) <br> [Datasheet](/lte-modem-and-usb-add-on-v2-datasheet/) |
| Microhard | Medium Range (1-3km) IP connectivity for control, telemetry and video | |

## Gimbal Specs (Optional Add-on)

### VOXL-m500-R2-G1

<span style="color:red">Camera not included</span>

For this gimbal, the recommended camera is something like the `Mobius Pro Mini Action Camera` based on form factor alone.  We are not qualifying this camera's performance, but based on size, weight and geometry, this camera has been shown to work well in our testing wih the gimbal referenced below.

|                     | Details |
| ---                 | ---     |
| Gimbal              | [HMG MA3D](https://hobbyking.com/en_us/hmg-ma3d-3-axis-brushless-gimbal-for-mobius-camera.html) |
| Regulator           | [D24V22F12](https://www.pololu.com/product/2855) |
| Control             | Pitch and yaw through AUX1 and AUX2 |
|                     | [See User Guide](/voxl-m500-gimbal-user-guide/) |
