---
layout: default
title: VOXL Functional Description
parent: VOXL Datasheet
grand_parent: Datasheets
nav_order: 1
permalink: /voxl-datasheet-functional-description/
---

# VOXL Functional Description
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

### High level specifications of the platform

- Base Operation System: Linux Yocto Jethro with 3.18 kernel. Additional Linux Operating Systems can be used by running Docker on VOXL, details [here](https://docs.modalai.com/docker-on-voxl/)
- Qualcomm Snapdragon 821 w/ 4GB LPDDR4 1866MHz 
  - [Snapdragon 821 Datasheet](https://developer.qualcomm.com/download/sd820e/qualcomm-snapdragon-820e-processor-apq8096sge-device-specification.pdf) 
  - [Snapdragon 821 docs](https://developer.qualcomm.com/hardware/apq-8096sg/tools)
- Quad-core CPU up to 2.15GHz
- Adreno 530 GPU at 624MHz
- Hexagon compute DSP (cDSP) 825MHz
- Hexagon sensor DSP (sDSP) 700MHz
- 4k30 Video Capture h.264/5 w/ 720p FPV
- PCB 36mm x 75mm, 12-layer, 1mm thick

### Camera

VOXL supports the following camera interfaces:
* MIPI-CSI2
* USB UVC
* HDMI

Please see the [Camera and Video Guides](https://docs.modalai.com/camera-video-guides/) for more detailed information.


### Connectivity and Location

- Pre-certified Wi-Fi module [QCNFA324 FCC ID:PPD-QCNFA324](https://fccid.io/PPD-QCNFA324)
  - QCA6174A modem
  - 802.11ac 2x2 Dual-band
  - Bluetooth 4.2 (dual-mode)
- 4G LTE as optional add-on module
- Microhard pDDL as an optional add-on module
- WGR7640 GNSS 10Hz
- I/O:
  - 1x USB3.0 OTG (ADB port)
  - 1x USB2.0 (expansion port)
  - 2x UART
  - 3x I2C
  - Additional GPIO and SPI can be configured

### Storage

- 32GB (UFS 2.0)
- Micro SD Card

### Software

- Docker with Ubuntu, Debian or Alpine
- OpenCV 2.4.11, 3.4.6, 4.2
- ROS Indigo
- Qualcomm Navigator 1.0 Flight Control
- Qualcomm Machine Vision SDK

[Top](#table-of-contents)

---

## Power Specifications

The VOXL should be powered by a supply rated at 6A and 5VDC.  

A recomended power supply is defined [here](../quickstarts/requirements.md).

- **Caution**: although the VOXL may power up from USB power, it is not recommended to operate this way.

[Top](#table-of-contents)

---

## Mechanical Specifications

### 3D STEP File

[3D STEP File](https://developer.modalai.com/asset/download/37)

### Board Dimensions

![voxl-core-imu-locations](../../images/datasheet/voxl-core-imu-locations-small.png)

[Top](#table-of-contents)

---

## On board sensors

| Configuration | VOXL (M0006) | VOXL Legacy aka Qualcomm Flight Pro (HA942) | Interface |
| --- | --- | --- | --- | 
| IMU0 | ICM20948 | MPU9250 | SPI10 |
| IMU1 (recommended) | ICM20948 | MPU9250 | SPI1 |
| Barometer | Bosch BMP280 | Bosch BMP280 | i2c3 |
| GNSS | Internal Snapdragon | Internal Snapdragon | TBD |

### IMU Notes

*Note:* IMU1 consistently performs better than IMU0. IMU1 is **strongly recommended** for flight and VIO use. SNAV 1.3 and greater, which is found at developer.modalai.com, uses IMU1 by default.


[Top](#table-of-contents)

---

## Recommended Operating Conditions

- Operating Temperature: -10°C to +70°C

[Top](#table-of-contents)
