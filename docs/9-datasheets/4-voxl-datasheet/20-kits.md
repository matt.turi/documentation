---
layout: default
title: VOXL Kits
parent: VOXL Datasheet
grand_parent: Datasheets
nav_order: 2
permalink: /voxl-kits/
---

# VOXL Kits
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## VOXL Development Kit

![voxl-dk](/images/quickstart/voxl/voxl-dk.jpg)

[Available for purchase here](https://www.modalai.com/voxl)

- [MKIT-00001 - VOXL Core Board Kit](/voxl-datasheet/)
- [MKIT-00037-1 - VOXL Power Module v3](/power-module-v3-datasheet/)
- MKIT-00003 - VOXL Wi-Fi Antenna Kit

