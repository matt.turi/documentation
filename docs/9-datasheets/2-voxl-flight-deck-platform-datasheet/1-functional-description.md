---
layout: default
title: VOXL Flight Deck Functional Description
parent: VOXL Flight Deck Platform Datasheet
grand_parent: Datasheets
nav_order: 1
permalink: /voxl-flight-deck-functional-description/
---

# VOXL Flight Deck Platform Functional Description
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL Flight Deck Platform is a fully assembled and calibrated flight kit, ready to mount to a vehicle and attach a battery!  It consists of the following core components:

- [VOXL Flight](/voxl-flight-datasheet/) computing platform mounted in VOXL-TRAY with:
  - [Stereo Sensors](/voxl-stereo-camera-datasheet/)
  - [Tracking Sensor](/voxl-tracking-camera-datasheet/)
  - [Hires Sensor](/voxl-hires-camera-datasheet/)
  - Wi-Fi Antennas, Cooling Fan, [RC inpout cable](/cable-datasheets/#mcbl-00005/)
- [VOXL Power Module Kit v2](/power-module-v2-datasheet/)
- PWM Breakout Kit (M0022 board and [MCBL-00004 cable](/cable-datasheets/#mcbl-00004/))

To see an example of the VOXL Fight Deck in use, check out the [VOXL-m500](/voxl-m500-functional-description/)!

## High-Level Specs

| Specicifcation | [VOXL-FLIGHT-DECK-R2-1](https://www.modalai.com/collections/voxl-development-kits/products/voxl-flight-deck-r1) |
| --- | --- |
| Take-off Weight | TBD |
| Computing | [VOXL Flight](/voxl-flight-datasheet/) |
| Flight Control | PX4 on [VOXL Flight](/voxl-flight-datasheet/) |
| Image Sensors | [Stereo](/voxl-stereo-camera-datasheet/), [Tracking](/voxl-tracking-camera-datasheet/), [4k High-resolution](/voxl-hires-camera-datasheet/) |
| Communication | WiFi built-in, [LTE](/lte-modem-and-usb-add-on-datasheet/) or [Microhard](/microhard-add-on-datasheet/) optional |
| Power Module | [Power Module v2](power-module-v2-datasheet) | 
| Battery | Up to 5S (not included, [recommended](https://www.thunderpowerrc.com/products/tp3400-4spx25)) |

## 2D/3D Drawings

[VOXL_FLIGHT_DECK_R1-NO_PCB.STEP](https://storage.googleapis.com/modalai_public/modal_drawings/VOXL_FLIGHT_DECK_R1-NO_PCB.STEP)

![voxl-flight-deck-cad](/images/datasheet/voxl-flight-deck/flight-deck-cad.png)
