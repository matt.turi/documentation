---
layout: default
title:  LTE Modem and USB Add-On v2 Datasheet
parent: VOXL Add-On Datasheets
grand_parent: Datasheets
nav_order: 1
has_children: false
permalink: /lte-modem-and-usb-add-on-v2-datasheet/
---

# LTE Modem and USB Add-on v2 Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

[Manual](/lte-v2-modem-manual/)

## Board Connections and I/O

![m0030-1-top.png](../../images/datasheet/voxl-lteh/m0030-1-top.png)

![m0030-1-bottom.png](../../images/datasheet/voxl-lteh/m0030-1-bottom.png)

## Specifications

### Physical Specification

| Specicifcation | Value |
| --- | --- |
| Weight (with modem and lid) | 14g |
| Dimensions | 37.0 x 43.3mm |
| Downstream USB ports | 2 |
| Cellular 3GPP Bands Supported | See `Radio Specifications` below |
| WWAN Antenna Connectors (MIMO) | UFL-R-SMT-110 |
| Recommend Antennas | [W3907B0100](https://www.digikey.com/product-detail/en/pulselarsen-antennas/W3907B0100/1837-1003-ND/7667474) |
| Downstream USB Connectors | 4 Position JST GH, Vertical, BM04B-GHS-TBT |
| Downstream USB Mating Connector | JST GHR-04V-S |
| Debug Console Connector | BM04B-SRSS-TB |
| Debug Console Mating Connector | SHR-04V-S-B |
| J1 Connector (bottom of board) | Samtec Inc, MPN: QTH-030-01-F-D-K |
| J1 Mating Connector | Samtec Inc., MPN: QSH-030-01-L-D-A-K |

![m0030-1-dim.png](../../images/datasheet/voxl-lteh/m0030-1-dim.png)

### Radio Specification

#### VOXL-ACC-LTEH-7607 (EMEA)

- [WP7607 Modem](https://www.sierrawireless.com/products-and-solutions/embedded-solutions/products/wp7607/)
- [HL SNAP-IN COVER](https://datasheet.octopart.com/HL-SNAP-IN-COVER-Sierra-Wireless-datasheet-66898230.pdf)

| Specification                | Details |
| ---                          | --- |
| 4G LTE, Cat-4                | B1, B3, B7, B8, B20, B28 |
| 3G Fallback, HSPA+, UMTS     | B1, B8 |
| 2G Fallback, EDGE, GSM, GPRS | 900, 1800 |
| Bandwidth                    |  up to 150/50 Mbps Download/Upload |

#### VOXL-ACC-LTEH-7610 (North America)

- [WP7610 Modem](https://www.sierrawireless.com/products-and-solutions/embedded-solutions/products/wp7610/)
- [HL SNAP-IN COVER](https://datasheet.octopart.com/HL-SNAP-IN-COVER-Sierra-Wireless-datasheet-66898230.pdf)

| Specification                | Details |
| ---                          | --- |
| 4G LTE, Cat-4                | B2, B4, B5, B12, B13, B14, B17, B66 |
| 3G Fallback, HSPA+, UMTS     | B2, B4, B5 |
| Bandwidth                    |  up to 150/50 Mbps Download/Upload |

### SIM Cards and Supported Networks

The modem card requires the use of a Nano SIM card.

AT&T (FirstNet), Verizon and  Sierra Wireless are supported by `VOXL-ACC-LTEH-7610`.  

For Band 3 support, the `VOXL-ACC-LTEH-7607` modem is required and Band 3 SIM cards need to be requested through ModalAI.

### Power Consumption

| Configuration | 5V Power Consumption (mA) |
| --- | --- |
| Baseline (USB Hub, Glue Logic, All LEDs ON)         | 150 |
| Baseline plus Qty-2 USB Peripherals at Max 500mA*   | 1180 |
| Baseline + Qty-2 DS USB* + WP Series^ HSDPA Band 1  | 2010 |
| Baseline + Qty-2 DS USB* + WP Series^ HSDPA Band 8  | 1930 |
| Baseline + Qty-2 DS USB* + WP Series^ LTE Band 1    | 2240 |
| Baseline + Qty-2 DS USB* + WP Series^ LTE Band 7    | 2150 |
| Baseline + Qty-2 DS USB* + WP Series^ LTE Band 25   | 2265 |

**For each of the two downstream peripherals removed from the hub, subtract 500mA plus ~30mA logic from the power requirement.*

*^ Band power consumption information is limited to WP7502, WP7504, and WP8548 (max of these three, at listed mode & band). For other module power consumptions or alternate bands, please contact your local Sierra Wireless FAE, or ModalAI.*

## LEDs

### Status LEDs

| LED | Description |
| --- | --- |
| `5VDC IN` | Illuminates `GREEN` when the board is powered |
| `WWAN STAT` |  Illuminates `GREEN` when the modem is initialized |
| `PWR GOOD` | Illuminates solid `GREEN` when all local power is good |

### USB LEDs

The `USB PORT2 PWR` and `USB PORT3 PWR` LEDs illuminate green to indicate the USB device is powered when plugged in

## Connector Pinouts

### J2 - Serial Debug Port

- Connector - BM04B-SRSS-TB
- Mating Connector - SHR-04V-S-B

| Pin | Description |
| --- | --- |
| 1 | 3.3 VDC
| 2 | UART_RX_3P3V |
| 3 | UART_TX_3P3V |
| 4 | GND |

### J5 - USB Port 2

- Connector - 4 Position JST GH, Vertical, BM04B-GHS-TBT
- Mating Connector - JST GHR-04V-S

| Pin | Description |
| --- | --- |
| 1 | 5VDC (500mA max) |
| 2 | D- |
| 3 | D+ |
| 3 | GND |

### J6 - USB Port 3

- Connector - 4 Position JST GH, Vertical, BM04B-GHS-TBT
- Mating Connector - JST GHR-04V-S

| Pin | Description |
| --- | --- |
| 1 | 5VDC (500mA max) |
| 2 | D- |
| 3 | D+ |
| 3 | GND |

## Environmental

### Operating Temperature

TODO

## 2D/3D Drawings

[M0030_REVA_WP_Socket_with_lid.stp](https://storage.googleapis.com/modalai_public/modal_drawings/M0030_REVA_WP_Socket_with_lid.stp)

## Emergency Boot Switch

Typically left in the `OFF` state, this switch can be used to enable `emergency boot` mode.  More information can be found [here](/usb-expander-and-debug-manual/#forcing-the-voxl-into-emergency-boot).
