---
layout: default
title: VOXL Image Sensor Datasheets
parent: Datasheets
nav_order: 6
has_children: true
permalink: /voxl-image-sensor-datasheets/
---

# VOXL Image Sensor Datasheets
{: .no_toc }

The VOXL supports as many as 4 MIPI image sensors concurrently. The following lists the supported image sensor modules.

## Supported Sensor Configurations

### 4k with Vision

| CSI | Sensor |
| --- | --- |
| CSI0 | [High-Resolution](https://docs.modalai.com/voxl-hires-camera-datasheet/) |
| CSI1 | [Stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) |
| CSI2 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) |

### Vision-only

| CSI | Sensor |
| --- | --- |
| CSI0 | No Connect |
| CSI1 | [Stereo](https://docs.modalai.com/voxl-stereo-camera-datasheet/) |
| CSI2 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) |

### Time of Flight (Coming Soon)

| CSI | Sensor |
| --- | --- |
| CSI0 | [High-Resolution](https://docs.modalai.com/voxl-hires-camera-datasheet/) |
| CSI1 | [TOF](https://docs.modalai.com/voxl-tof-sensor-datasheet/) |
| CSI2 | [Tracking](https://docs.modalai.com/voxl-tracking-camera-datasheet/) |


{:toc}
