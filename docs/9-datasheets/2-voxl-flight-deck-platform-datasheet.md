---
layout: default
title: VOXL Flight Deck Platform Datasheet
parent: Datasheets
nav_order: 2
has_children: true
permalink: /voxl-flight-deck-platform-datasheet/
---


# VOXL Flight Deck Datasheet
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

A fully assembled and calibrated flight kit. Pre-configured for GPS-denied navigation using Visual Inertial Odometry (VIO).

![voxl-flight-deck](/images/datasheet/voxl-flight-deck/flight-deck-kit.jpg)

![voxl-flight-deck-cad](/images/datasheet/voxl-flight-deck/flight-deck-cad.png)
