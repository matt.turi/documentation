---
layout: default
title: VOXL Flight Configuration
parent: VOXL Flight Datasheet
grand_parent: Datasheets
nav_order: 10
permalink: /voxl-flight-datasheet-configuration/
---

# VOXL Flight Configuration
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## QGroundControl

Configuration of the VOXL Flight is fully supported by QGroundControl 4.0.

## PX4 Parameter Files

Parameter files are available for reference:

- <https://gitlab.com/voxl-public/px4-parameters>

## PX4 System Defaults

The following describe the system defaults, as specified by `boards/modalai/fc-v1/init/rc.board_defaults`

### MAVLink Instance 0

- configured by default as TELEM1 (exposed at`J1010`) at 57600 baud in Normal mode
- this is configurable by the user

### MAVLink Instance 1

- hard coded to TELEM2 as it's routed in the PCB, defaulted at 921600 baud in Normal mode
- this is the primary serial interface to communicate with VOXL
- the baud rate and mode are configurable by the user, but recommended to use the defaults

### Safety Switch Disabled

The saftety switch can be 'pressed' by pulling `J1011` pin 5 up to 3.3V (which is provided by J13 pin 1).  By default, we have the safety switch disabled, but it can be enabled via QGroundControl by setting `CBRK_IO_SAFETY` to 0.
