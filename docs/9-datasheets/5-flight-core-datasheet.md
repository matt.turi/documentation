---
layout: default
title: Flight Core Datasheet
parent: Datasheets
nav_order: 5
has_children: true
permalink: /flight-core-datasheet/
---

# Flight Core Datasheet
{: .no_toc }

The ModalAI Flight Core is an STM32F7-based flight controller for PX4, made in the USA.  The Flight Core can be paired with VOXL for obstacle avoidance and GPS-denied navigation, or used independently as a standalone flight controller.

![fc-top-with-quarter](/images/datasheet/modalai-fc-v1-quarter.jpg)

