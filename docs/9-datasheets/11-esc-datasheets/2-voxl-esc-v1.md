---
layout: default
title: VOXL ESC v1 Datasheet
parent: ESC Datasheets
grand_parent: Datasheets
nav_order: 2
permalink: /voxl-esc-v1-datasheet/
---

# VOXL ESC v1 Datasheet (Work in Progress)
{: .no_toc }

---

## Overview

|                 | Details     |
|---              |---          |
| Input           | 2-3 S Li-Po |
|                 | XT30 connector |
| Output          | 4 channels at 8A maximum continuous current per channel |
|                 | Maximum RPM : 17,000RPM for a 12-pole motor (14,500RPM for a 14-pole motor) |
|                 | 5VDC Power Regulator Output at 6A supporting VOXL Flight, VOXL and Flight Core |
|                 | Motor Connectors: Hirose [DF3-3S-2C](https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/DF3-3S-2C/H2084-ND/141482) |
|                 | Regulated Output Connector:  Molex Inc, MPN: [22-05-7045](https://www.digikey.com/products/en?keywords=22-05-7045)
|                 | LED Connector:  [DF13A- 4P-1.25H(50)](https://www.digikey.com/products/en?keywords=DF13A-4P-1.25H(50))
| Features        | Open-loop control (set desired % power) |
|                 | Closed-loop RPM control (set desired RPM), used in PX4 driver |
|                 | Control LED colors |
|                 | Generate tones using motors |
|                 | Real-time RPM feedback |
| Communications  | Supported by VOXL Flight, VOXL and Flight Core |
|                 | Bi-directional UART (5VDC logic-level, 3.3VDC supported)
| PX4 Integration | Scheduled to be supported in PX4 1.12 |
|                 | Available [here in developer preview](https://github.com/modalai/px4-firmware/tree/modalai-esc) |
| Resources       | [Manual](/voxl-esc-v1-manual/) |
|                 | [User Guide](/voxl-esc-px4-user-guide/) |

## Details

Power Input
- 2-3 S Li-Po
- reverse polarity protection: yes
- on/off switch input : yes
- auxiliary input : yes (only powers ESC MCUs using diode OR, but does not power Mosfets — useful for quick hot-swapping of main battery)

Communication
- All four ESCs communicate via same bi-directional UART port
- boot loader 38400 baud rate, firmware 250000 baud rate (firmware baud rate is adjustable)
- [protocol specification](https://developer.qualcomm.com/qfile/34042/80-p4698-19_b_qualcomm_snapdragon_navigator_esc_protocol_spec.pdf)
- [protocol sample implementation](https://source.codeaurora.org/external/qti-robotics/esc-driver/)
- I2C communication is available on 4-pin Molex connector and is used only by LTC2946 (supported in PX4 as `voxlpm` driver)
- PWM input : not available

5V Power Regulator Output
- 5.15V @ 6A

Performance Specifications
- Commutation Method : 6-step 
- Maximum RPM : 17,000RPM for a 12-pole motor (14,500RPM for a 14-pole motor).
- Maximum continuous current per channel: >8A (thermally limited, need to test in specific application)
- Command update rate > 500Hz (500Hz recommended)

Voltage and Current Sensing
- LTC2946 high precision voltage and total current sensing, available via separate i2c
- each MCU on ESC measures it’s own voltage and reports via UART feedback packets
- individual channel current sensing is not available

LEDs
- each ESC has control over a low-power RGB LED. Color can be set via UART interface.
- on-board status LED (blue) for each MCU

Features
- Open-loop control (set desired % power)
- Closed-loop RPM control (set desired RPM)
- Control LED colors
- Generate tones using motors
- Real-time RPM feedback

Board Specifications
- Dimensions: TODO
- MCU: ATMEGA328P @ 20Mhz
- Mosfet type : N + P
- Switching Frequency: 20Khz

Connectors:
- Motor outputs: Hirose [DF3-3S-2C](https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/DF3-3S-2C/H2084-ND/141482)
- 5V output : Molex Inc, MPN: [22-05-7045](https://www.digikey.com/products/en?keywords=22-05-7045)

## Board Connections and Pin-out Specifications

### VOXL ESC v1 Board Top

![m0004-top.png](/images/datasheet/escs/m0004/m0004-top.png)

### VOXL ESC v1 Board Bottom

![m0004-bottom.png](/images/datasheet/escs/m0004/m0004-bottom.png)

### Motor CHX

- Connector: Hirose [DF3-3S-2C](https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/DF3-3S-2C/H2084-ND/141482)
- Mating Connector: [DF3-3EP-2C](https://www.digikey.com/product-detail/en/hirose-electric-co-ltd/DF3-3EP-2C/H4036-ND/151330)

| Pin | Signal Name |
|--- |--- |
| 1 | CHX_PA_M |
| 2 | CHX_PB_M |
| 3 | CHX_PC_M |

### J1 - CH2_LED

- Connector: DF13A- 4P-1.25H(50)
- Mating Connector: DF13-4S-1.25C

| Pin | Signal Name |
|--- |--- |
| 1 | GND |
| 2 | CH2_BLU |
| 3 | CH2_GRN |
| 4 | CH2_RED |

### J2 - CH3_LED

- Connector: DF13A- 4P-1.25H(50)
- Mating Connector: DF13-4S-1.25C

| Pin | Signal Name |
|--- |--- |
| 1 | GND |
| 2 | CH3_BLU |
| 3 | CH3_GRN |
| 4 | CH3_RED |

### J6 - CH4_LED

- Connector: DF13A- 4P-1.25H(50)
- Mating Connector: DF13-4S-1.25C

| Pin | Signal Name |
|--- |--- |
| 1 | GND |
| 2 | CH4_BLU |
| 3 | CH4_GRN |
| 4 | CH4_RED |

### J7 - CH1_LED

- Connector: DF13A- 4P-1.25H(50)
- Mating Connector: DF13-4S-1.25C

| Pin | Signal Name |
|--- |--- |
| 1 | GND |
| 2 | CH1_BLU |
| 3 | CH1_GRN |
| 4 | CH1_RED |

### J9 - Flight Controller UART

- Connector: Hirose Electric, MPN: DF13-6P-1.25H(50)
- Mating Connector: DF13-6S-1.25C cable assemblies (MCBL-00013)

| Pin | Signal Name |
|--- |--- |
| 1 | NC |
| 2 | RX (5V, supports 3.3V) |
| 3 | TX (5V, supports 3.3V) |
| 4 | NC |
| 5 | GND |
| 6 | NC |

### J10 - Power Module Output

- Connector: Molex Inc, MPN: 22-05-7045
- Mating Connector: 50-37-5043 based cables, MCBL-00001 (also available [here](http://www.robotis.us/robot-cable-4p-140mm-10pcs/))

| Pin | Signal Name |
|--- |--- |
| 1 | 5VDC |
| 2 | GND|
| 3 | I2C_SCL |
| 4 | I2C_SDA |
