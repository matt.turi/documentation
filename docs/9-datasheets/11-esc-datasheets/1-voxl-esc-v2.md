---
layout: default
title: VOXL ESC v2 Datasheet
parent: ESC Datasheets
grand_parent: Datasheets
nav_order: 1
permalink: /voxl-esc-v2-datasheet/
---

# VOXL ESC v2 Datasheet
{: .no_toc }

---

## Hardware Overview
![modalai_esc_reva_labels.jpg](/images/datasheet/escs/m0027/m0027a-labels.jpg)

## Dimensions
![modalai_esc_reva_dims.png](/images/datasheet/escs/m0027/m0027a-dims.png)

## Specifications

|                 | Details     |
|---              |---          |
| Power Input     | 2-4 S Li-Po (5.5-18V) |
|                 | XT30 connector (suggested) |
| Power Output    | Dual AUX power output at 4.5V (adjustable) 600mA (can be used for Neopixel LEDs) |
| Performance     | 4 channels at 10A+ maximum continuous current per channel (depends on cooling) |
|                 | Maximum RPM : 50,000+RPM for a 12-pole motor |
| Features        | Open-loop control (set desired % power) |
|                 | Closed-loop RPM control (set desired RPM), used in PX4 driver |
|                 | Control LED colors of external LED Strip (Neopixel) |
|                 | Generate tones using motors |
|                 | Real-time RPM feedback |
| Communications  | Supported by VOXL Flight, VOXL and Flight Core |
|                 | Dual Bi-directional UART (3.3VDC logic-level) |
|                 | PWM input supporting 1-2ms and OneShot125 (more protocols coming soon) |
| Connectors      | UART: Hirose DF13 6-pin |
|                 | PWM Input: JST GH 6-pin |
|                 | AUX Regulated Output Connectors:  N/A (solder pads) |
|                 | Motor Output Connectors:  N/A (solder pads) |
| Hardware        | MCU : STM32F051K86 @ 48Mhz, 64KB Flash |
|                 | Mosfet Driver : MP6530 |
|                 | Mosfets	: AON7528 (N-channel) |
|                 | Individual Current Sensing : 4x 2mOhm + INA186 |
|                 | ESD Protection : Yes (on UART and PWM I/O)  |
|                 | Temperature Sensing : Yes |
|                 | On-board Status LEDs : Yes |
|                 | Weight (no wires) : 9.5g |
|                 | Motor Connectors: N/A (solder pads) |
| PX4 Integration | Scheduled to be supported in PX4 1.12 |
|                 | Available [here in developer preview](https://github.com/modalai/px4-firmware/tree/modalai-esc) |
| Resources       | [Manual](/voxl-esc-v2-manual/) |
|                 | [PX4 Integration User Guide](/voxl-esc-px4-user-guide/) |
