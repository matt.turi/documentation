---
layout: default
title: VOXL m500 Reference Drone Datasheet
parent: Datasheets
nav_order: 1
has_children: true
permalink: /voxl-m500-reference-drone-datasheet/
youtubeId: qwSKEOF2rFM
---

# VOXL m500 Snapdragon Reference Drone Datasheet
{: .no_toc }

{:toc}

## Overview

A fully assembled, calibrated and tested Qualcomm Snapdragon development drone for R&D and VOXL evaluation purposes.

[Buy here](https://www.modalai.com/m500)

{% include youtubePlayer.html id=page.youtubeId %}
