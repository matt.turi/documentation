---
layout: default
title: VOXL ESC v1
parent: Electronic Speed Controller (ESCs)
nav_order: 2
permalink: /voxl-esc-v1-manual/
---

# VOXL ESC v1 Manual
{: .no_toc }

## Overview

- [User Guide](/voxl-esc-px4-user-guide/)
- [Datasheet](/voxl-esc-v1-datasheet/)

## Channel Legend

The following is a typical 'mapping' used with PX4

### Motors

| Silkscreen | ESC ID | Motor Number | PX4 Motor Number |
|--- |--- |--- |--- |
| "CH0" | 0 | 1 | 3 |
| "CH1" | 1 | 2 | 2 |
| "CH2" | 2 | 3 | 4 |
| "CH3" | 3 | 4 | 1 |

### LEDs

| Connector | LED Number | PX4 Motor Number |
|--- |--- |--- |
| J7 | 1 | 3 |
| J1 | 2 | 2 |
| J2 | 3 | 4 |
| J6 | 4 | 1 |

### Installation Example

- J10 - Connects to VOXL Flight J1003 (shown on next graphic) using [MCBL-00001](/cable-datasheets/#mcbl-00001)
- M1 - Motor 1 output, wired PX4 Motor 3 ("P3")
- M2 - Motor 2 output, wired PX4 Motor 2 ("P2")
- M3 - Motor 3 output, wired PX4 Motor 4 ("P4")
- M4 - Motor 4 output, wired PX4 Motor 1 ("P1")

![voxl-v1-esc-manual-top.png](/images/userguides/escs/voxl-v1-esc-manual-top.png)

- J1 - Connects to LED2 ("L2")
- J2 - Connects to LED3 ("L3")
- J6 - Connects to LED4 ("L4")
- J7 - Connects to LED1 ("L1")
- J9 - Connects to VOXL Flight J1002 using [MCBL-00013](/cable-datasheets/#mcbl-00013)

![voxl-v1-esc-manual-bottom.png](/images/userguides/escs/voxl-v1-esc-manual-bottom.png)
