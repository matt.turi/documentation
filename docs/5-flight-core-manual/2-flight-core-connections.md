---
layout: default
title: Flight Core Connections
parent: Flight Core Manual
nav_order: 2
has_children: false
permalink: /flight-core-connections/
---

# Flight Core Connections
{: .no_toc }

## Setting up the Connections

Before configuring the Flight Core's software, we suggested mounting it to your airframe and make sure the airframe is physically ready to fly. You should connect at least the following items to proceed through this getting-started manual.

### USB

- Using the [MCBL-00006](/cable-datasheets/#mcbl-00006) cable included in the `FC-R1` kit, you can connect to a host computer running QGroundControl over USB.

  - **NOTE** - it's possible to inadvertently plug the USB cable into the wrong port which can lead to issues.  Please be cautious here!

![flight-core-usb.png](/images/flight-core/flight-core-usb.png)

### ESCs/Motors

- Using the [MCBL-00004](/cable-datasheets/#mcbl-00004) cable included in the `FC-R1` kit, connect the Flight Core board's `J7` to the PWM Breakout Board
- From the PWM Breakout Board, you can connect to your ESCs/Motors

![flight-core-pwm.png](/images/flight-core/flight-core-pwm.png)

### Power Supply

- Using the [MCBL-00003](/cable-datasheets/#mcbl-00003) cable included in the `FC-PWR` kit, connect the Power Module to the Flight Core's `J6` connector
- Connect the Power Modules's output to your power distribution method (e.g. for an s500 airframe, you'd connect to the PCB Center plate)

![flight-core-power.png](/images/flight-core/flight-core-power.png)

### RC Receiver

- Using the [MCBL-00005](/cable-datasheets/#mcbl-00005) cable included in the `FC-R1` kit, connect to an RC Receiver (e.g. a Spektrum Satellite) to Flight Core's `J12`.

![flight-core-rc.png](/images/flight-core/flight-core-rc.png)

- Additionally, not included in the kit, you can use [MCBL-00018](/cable-datasheets/#mcbl-00018) to connect an FrSky receiver like the X8R.

![flight-core-frsky.png](/images/flight-core/flight-core-frsky.png)

### GPS

If you wish to use a GPS/magnetometer you should attach it now as we will reach the calibration step shortly.

A DroneCode standard 6-pin GPS can be used and connected to Flight Core's `J10`.  For example, the [mRo GPS u-Blox Neo-M8N Dual Compass LIS3MDL+ IST8310](https://store.mrobotics.io/mRo-GPS-u-Blox-Neo-M8N-HMC5983-Compass-p/mro-gps003-mr.htm) or [Pixhawk4 2nd GPS Module](https://shop.holybro.com/pixhawk4-2nd-gps-module_p1145.html)

![flight-core-gps.png](/images/flight-core/flight-core-gps.png)

### Wireless Telemetry Link

#### VOXL

We recommend using a VOXL as your wireless telemetry link, which supports both WiFi and LTE.  If you intend to use a VOXL as the primary wireless telemetry connection, you should make sure it's software is up to date has [voxl-vision-px4 installed and configured](/voxl-vision-px4-installation/) with the IP address of your ground control computer already set.

Captured in the [how to connect VOXL to Flight Core](/how-to-connect-voxl-to-flight-core/) section, you can see how to use the [MCBL-00007](/cable-datasheets/#mcbl-00007) cable to connect the Flight Core's `J1` to VOXL's `J12`

#### SiK Based Telemetry Radio

Alternatively, you can use a SiK point-to-point telemetry link like the [Transceiver Telemetry Radio V3](http://www.holybro.com/product/transceiver-telemetry-radio-v3-915mhz/) using the included cables in that kit and connect to Flight Core's DroneCode standard `J5` connector

![flight-core-sik.png](/images/flight-core/flight-core-sik.png)
