---
layout: default
title: Flight Core Mounting and Connections
parent: Flight Core Getting Started
grand_parent: Flight Core Manual
nav_order: 1
permalink: /flight-core-mounting-and-connections/
---

# Flight Core Mounting and Connections
{: .no_toc }

## Summary

Before configuring the Flight Core's software, we suggested mounting it to your airframe and make sure the airframe is physically ready to fly. You should connect at least the following items to proceed through this getting-started manual.

- Motors/ESCs
- Flight Core Power Module
- RC Receiver (e.g. Spektrum Satellite)
- Wireless Telemetry Link, either:
  - VOXL running voxl-vision-px4
  - or SiK 915mhz point-to-point telemetry link

If you wish to use a GPS/magnetometer you should attach it now as we will reach the calibration step shortly.

If you intend to use a VOXL as the primary wireless telemetry connection, you should make sure it's software is up to date has [voxl-vision-px4 installed and configured](/voxl-vision-px4-installation/) with the IP address of your ground control computer already set.

## Flight Core Kit Contents

Here's what you'll get in the Flight Core Kits, which contain the necessary hardware and cables needed to fly provided you have an airframe, motors, ESCs and a battery!

### Flight Core R1

The `FC-R1` kit contains the following:

- Flight Core PX4 board
- PWM Breakout Board and cable (`MCCA-M0009` and `MCBL-00004`)
- RC Receiver (Spektrum Satellite) cable (`MCBL-00005`)
- JST-to-USBA cable (`MCBL-00006`)

**TODO: Picture Here**

### Flight Core Power Adapter

The `FC-PWR` kit contains the following:

- 5VDC Power Module (`MCCA-M0009`) (handles up to 6S input voltage) with battery voltage and current monitoring
- Power adapter cable (`MCBL-00003`)

**TODO: Picture Here**

## Next Steps

The [Flight Core Connections](/flight-core-connections/) are described here, which will show you how to connect everything in detailed documentation.

The Flight Core ships with the latest PX4 firmware available at the time, but you may want to [update Flight Core firmware](/update-flight-core-firmware/) depending on your needs.
