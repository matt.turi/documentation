---
layout: default
title: Configure RC Radio
parent: Flight Core Getting Started
grand_parent: Flight Core Manual
nav_order: 5
permalink: /configure-rc-radio/
---

# Configure RC Radio

You are welcome to use any radio you like. For testing we use a Spektrum DX8 radio (e.g. SPM8000/SPMR8000) with a CLEAN model, e.g. no mixing, no trimming, default limits. By default, the Spektrum DX8 exposes channels 6 and 7 on the two 3-position toggle switches to the left and right of the "DX8" logo respectively. These are easy to reach switches which we assign to flight mode and motor-kill respectively.

## 7. Bind Radio to Receiver

Instructions on how to bind a Spektrum radio can be found [here](https://docs.px4.io/v1.9.0/en/config/radio.html).

<span style="color:red">**NOTE: After binding the radio, you will need to reset PX4 (or power cycle the unit) before proceeding with calibration**</span>

## 8. Calibrate Radio Channels

Now follow the on-screen instructions to calibrate the range and trims of your radio.

![8-calibrate-radio.png](/images/qgc/8-calibrate-radio.png)

Note that the radio calibration values are saved as PX4 parameters which will NOT be overwritten if you load any of our "trimmed" PX4 parameter files as described in the [Upload PX4 Parameters](/upload-px4-parameters/) page.

## 9. Set Flight Modes

Obviously every user will want to use different flight modes and different switch assignments, but for getting started with VOXL and and Flight Core we suggest starting with something similar to this configuration and working from there.

 * "Flap Gyro" switch left of Spektrum Logo
  * Channel 6
  * Up position:     Manual Flight Mode
  * Middle Position: Position Flight Mode
  * Down Position:   Offboard Flight mode

 * "Aux2 Gov" switch right of Spektrum Logo
  * Channel 7
  * Up position:     Motor Kill Switch Engaged
  * Down Position:   Motor Kill Switch Disengaged (required to fly)

Since we have a manual kill switch on the radio there is no need for the "safety switch" on the Pixhawk GPS module as so it is disabled in our params file in favor of the kill switch.

![9-flight-mode-config.png](/images/qgc/9-flight-mode-config.png)

## Next Steps

Next it's time to finish [miscellaneous PX4 configuration](/fc-configure-misc/).
