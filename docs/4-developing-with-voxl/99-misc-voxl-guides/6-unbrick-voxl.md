---
layout: default
title: Unbrick VOXL and VOXL Flight
parent: Misc VOXL Guides
grand_parent: Developing with VOXL
nav_order: 6
permalink: /unbrick-voxl/
---

# Unbricking Procedure for VOXL and VOXL Flight
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The System Image flashing process requires the device to be in `fastboot` mode.  The typical process uses `adb` to put the VOXL into `fastboot` mode over USB.  If the device is bricked and one cannot get the VOXL into `fastboot` using USB, then you'd follow this process before running the `install.sh` script as described in the [Flash System Image](/flash-system-image/) process.

The following VOXL add-on boards support the force fastboot mode switch:
* USB and Serial Debug Expander ([datasheet](https://docs.modalai.com/usb-expander-and-debug-manual))


### Force VOXL into fastboot

To force the VOXL to boot into `fastboot` mode:

- Power off the VOXL
- Attach the USB Expander and Debug Add-On board
- Ensure the `FASTBOOT` labeled switch is set to **on**
- Ensure the `EMERG BOOT` labeled switch is set to **off**
- Power on the VOXL
- **Immediately** run the program to flash the system image
  - if the fastboot connection drops, repeat the procedure but immediately after power on, set the `FASTBOOT` switch to **off**

The VOXL will stay in `fastboot` mode for approximately 10 seconds.  During this time, you can query for the device from a Linux system with the following command:

```bash
fastboot devices
```

You should see an output like:

```bash
a0259b4a    fastboot
```

**NOTE:** If the fastboot connection drops in the middle of the flashing process, try running the procedure above, but immediately after power on, toggle the `FASTBOOT` switch back to **off**

### Reflashing System Image after Forcing the VOXL into Fastboot

To reflash the System Image, you need to run the `install.sh` program within 10 seconds of powering up the VOXL into fastboot mode.  Forcing the VOXL into fastboot mode is covered in the previous section.  Flashing the System Image using `install.sh` is [covered in this section](/flash-system-image/).

