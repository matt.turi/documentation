---
layout: default
title: VOXL ToF Sensor User Guide
parent: Camera and Video Guides
grand_parent: Developing with VOXL
nav_order: 23
permalink: /voxl-tof-sensor-user-guide/
---

# VOXL ToF Sensor User Guide
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![VOXL ToF](/images/userguides/voxl-tof-sensor/voxl-tof.png)

## Requirements

- VOXL or VOXL-Flight with [VOXL System Image](/voxl-system-image/) 2.3.0 or newer
- VOXL ToF Sensor ([available here](https://www.modalai.com/products/voxl-dk-tof))
- Linux with Rviz
- [Setup ROS on Host PC](https://docs.modalai.com/setup-ros-on-host-pc/)
- [Setup ROS on VOXL](https://docs.modalai.com/setup-ros-on-voxl/)

## Hardware

- Ensure VOXL/VOXL-Flight is powered off
- Connect to VOXL/VOXL-Flight connector `J3` as shown

![VOXL ToF Install](/images/userguides/voxl-tof-sensor/voxl-tof-install-0.png)

## Guide

Below describes a method to quickly validate the hardware connections and system.

### Connect to VOXL Over Wi-Fi

- Setup and connect VOXL/VOXL-Flight as an [access point](/wifi-setup/)
- SSH onto the target:

```bash
ssh root@192.168.8.1
oelinux123
```

### Start roscore on VOXL

On VOXL:

```bash
# view network information
ifconfig
```

- This will give you the IP address of the VOXL, configure ROS with this:

```bash
export ROS_IP=IP-ADDR-OF-VOXL
source /opt/ros/indigo/setup.bash
roscore &
```

- This will print a line that should read something like this: ROS_MASTER_URI=http://AAA.BBB.CCC.DDD:XYZUV/
  - *Note:* The HTTP address will be different

### Conifugre Cameras on VOXL

On VOXL:

- Ensure the environment variables are setup by runnin the following commands:

```bash
voxl-configure-cameras
```

- Select the option that best fits your setup, For camera-id please check [here](https://docs.modalai.com/camera-connections/)
  - *Note:* for standalone testing, you can select option 5 `tof + tracking`

- Update environment:

```bash
exec bash
```

### Configure ROS On Desktop

On Linux desktop:

- Find IP address

```bash
# view network information
ifconfig
```

- Configure ROS environment variables, where the ROS_MASTER_URI is what you saw on the VOXL when you ran `roscore &` in the above step

```bash
export ROS_IP=IP-ADDR-OF-PC
export ROS_MASTER_URI=http://AAA.BBB.CCC.DDD:XYZUV/
```

- source variables, where xxxxxx is the ROS version you have installed

```bash
source /opt/ros/xxxxx/setup.bash
```

- The 2 transforms (for both laser scan and point cloud) are in the tof.launch file
  - In RViz choose Fixed Frame toption as /base_link instead of map
  - Open your .rviz file (by default in /home/xxx/.rviz/default.rviz)

```bash
vi /home/xxx/.rviz/default.rviz
```

- Change the "Fixed Frame:" option from "map" to "base_link"
  - Global Options: Fixed Frame: /base_link

### Launch ToF Node on VOXL

```bash
roslaunch /opt/ros/indigo/share/voxl_hal3_tof_cam_ros/launch/tof.launch
```

- Ensure the ROS node launches OK:

![ToF Node](/images/userguides/voxl-tof-sensor/voxl-tof-node-launch.png)

### Launch RViz on Desktop

- On the host computer, run RViz

```bash
rviz
```

- On the leftmost column click on the "Add" button
- In the pop-up options click on "Image"
- Change Display Name to "IR-Image"
- In the left column under "IR-Image" tab select type in Image Topic as /voxl_ir_image_raw
- Click on the "Add" button again
- In the pop-up options click on "Image"
- Change Display Name to "Depth-Image"
- In the left column under "Depth-Image" tab select type in Image Topic as /voxl_depth_image_raw
- "Add" PointCloud2 with Topic as "/voxl_point_cloud"
- "Add" LaserScan with Topic as "/voxl_laser_scan"

![ToF Visualized](/images/userguides/voxl-tof-sensor/voxl-tof-visualized.png)
