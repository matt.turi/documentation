---
layout: default
title: Stream and Control DSLR Cameras
parent: Camera and Video Guides
grand_parent: Developing with VOXL
nav_order: 8
permalink: /voxl-libgphoto2/
---

# How To Stream and Control Popular DSLR Cameras to your VOXL-based Drone
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

[voxl-libgphoto2](https://gitlab.com/voxl-public/voxl-libgphoto2) enables streaming a live video feed from a wide range of digital cameras to [QGroundControl](http://qgroundcontrol.com/). The following software is used to make this possible.

[gPhoto2](http://www.gphoto.org/) is a free, redistributable, ready to use set of digital camera software applications for Unix-like systems, supporting more than [2,500 cameras](http://www.gphoto.org/proj/libgphoto2/support.php). These cameras include popular options like Sony A6500, Panasonic DC-GH5, Nikon DSC, Nikon CoolPix, Canon PowerShot, and many more!

[FFmpeg](https://ffmpeg.org/) is a complete, cross-platform solution to stream video. FFmpeg has a large number of options that can be used to tune the video stream to your liking. On top of the default video encoders, voxl-libgphoto2 supports software encoding using [libx264](https://trac.ffmpeg.org/wiki/Encode/H.264) and hardware encoding using [OpenMax](https://trac.ffmpeg.org/wiki/HWAccelIntro).

## Building voxl-libgphoto2

### Installing necessary files

```
# On host PC
$ git clone git@gitlab.com:voxl-public/voxl-libgphoto2.git

$ cd voxl-libgphoto2

# With VOXL connected to host PC via. ADB
$ ./push-files-to-target
```

### Building Docker Image

```
# ADB over to VOXL
$ adb shell

# Enable station mode in order to download dependencies
$ voxl-wifi station [ssid] [password]

# Reboot to save changes
$ reboot
```

After VOXL has rebooted

```
# ADB over to VOXL
$ adb shell

# Move to Docker directory
$ cd /data/docker

# Build Docker image
$ docker build -t libgphoto2 libgphoto2/
```

After build has completed, switch WiFi mode to SoftAP

``` 
$ voxl-wifi softap [SSID]

# Reboot to save changes
$ reboot
```

## Hardware Setup

In order to stream a video feed to QGroundControl, voxl-libgphoto2 requires the use of a [Flight Core](https://www.modalai.com/products/flight-core) or [VOXL Flight](https://www.modalai.com/4g-add-on).

The following additional hardware is required to stream video from a digital camera through VOXL to QGroundControl.

| Part Number | Description | Link |
| --- | --- | --- |
| MCBL-00009-1 | USB Cable (Host, 4-pin JST to USB 2.0 Type A Female) | [Purchase](https://www.modalai.com/products/usb-cable-4-pin-jst-to-usb-a-female)
| Camera USB Cable | USB Cable included with camera e.g. (USB-C, Mini USB, Micro USB) | 

One of the following VOXL Add-on boards is required.

| Part Number | Description | Link |
| --- | --- | --- |
| VOXL-ACC-MH | VOXL Microhard Modem Add-on | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-microhard-modem-usb-hub)
| VOXL-ACC-HUB | VOXL USB Expansion Board with Fastboot and Emergency Boot | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-usb-expansion-board-with-debug-fastboot-and-emergency-boot)
| VOXL-ACC-LTEH | VOXL Cellular LTE and USB hub add-on  | [Purchase](https://www.modalai.com/collections/voxl-add-ons/products/voxl-lte?variant=32449203601459)

- Before beginning setup, make sure the VOXL is disconnected from both it's power source and it's USB to host PC cable

- Attach the add-on board to the VOXL and make sure that the boot switch(es) are turned off or to the left

- Plug the jst side of the MCBL-00009-1 USB Cable into the add-on board

- Plug the camera's USB cable into both the camera and the USB female side of the MCBL-00009-1 USB cable

- Power the VOXL 

![MH Standalone](/images/userguides/dslr-setup.jpg)

## Running voxl-libgphoto2

### Starting Docker container

Run a container with the following parameters in order to give voxl-libgphoto2 access to the camera via. it's USB port.

```
$ adb shell

$ docker run -i -t --rm -v /dev/bus/usb/:/dev/bus/usb --net=host --privileged libgphoto2
```

### Testing gPhoto2 functionality

The [gPhoto docs](http://www.gphoto.org/doc/manual/using-gphoto2.html) provide a good explanation on how to use the gPhoto command line interface

Here are a few examples:

```
$ gphoto2 --list-ports
Devices found: 4
Path                             Description
--------------------------------------------------------------
ptpip:                           PTP/IP Connection               
ip:                              IP Connection                   
serial:                          Serial Port Device              
usb:001,005                      Universal Serial Bus
```

In this example we can see that gPhoto has detected a USB bus on the device. However, we cannot tell yet whether we will have write access to the USB device file your camera will be assigned by the operating system.

The next step is to connect your camera and find out whether gPhoto can find it. This only works with USB.

```
$ gphoto2 --auto-detect
Model                          Port                                         
----------------------------------------------------------
Canon EOS 700D                 usb:001,005
```

In this case, a camera called "Canon EOS 700D" has been detected on your system's USB bus.

Next, we can test capturing and image.

```
$ gphoto2 --capture-image-and-download
New file is in location /capt0000.cr2 on the camera                         
Saving file as capt0000.cr2
Deleting file /capt0000.cr2 on the camera
```

This command can be used to take a picture and immediately save the image to the host Docker container instead of the camera.

### Streaming video using voxl-libgphoto2

In order to stream a live video feed from the digital camera to QGroundControl, use the following:

```
$ ./voxl-libgphoto2 192.168.8.XXX
```

The referenced IP address should be the IP address of the host PC running QGroundControl.

- On the host system, while connected to the VOXL's WiFi network, run QGroundControl

- In QGroundControl, press the purple QGC logo in the top left corner in order to access the `Application Settings` menu

- Under the `General` tab, scroll down until you find the `Video` section

- Under the `Video Source` dropdown, choose `MPEG-TS (h.264) Video Stream`

- In the `UDP Port` field, enter the default port: `4242`

- You will now be able to view the video stream under QGroundControl's `Fly` view

## Customizing FFmpeg parameters

By default, voxl-libgphoto2 uses the following basic FFmpeg parameters, specified in the [voxl-libgphoto2.py](https://gitlab.com/voxl-public/voxl-libgphoto2/-/blob/master/voxl-libgphoto2.py#L42) script:

```
$ ffmpeg -i - -c:v libx264 -f mjpeg udp://192.168.8.XXX:4242
```

These parameters can be modified to change the stream's encoder, resolution, bitrate, framerate, etc. as needed. 

## ModalAI Tested Cameras

The following cameras were tested by ModalAI and successfully used to stream video to QGroundControl:

- Canon EOS 700D
- Canon Powershot G7 X Mark III
