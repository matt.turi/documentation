---
layout: default
title: Image Streamer
parent: Camera and Video Guides
grand_parent: Developing with VOXL
nav_order: 2
permalink: /image-streamer/
---

# Image Streamer
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

This user guide describes how to use the cameras with the `voxl-cam-manager` applications as a means to validate the camera connections.  We'll use an application on the VOXL to stream video to a desktop Python based application.

VOXL Cam Manager is included as part of the [VOXL Software Bundle](/voxl-supporting-software/). The source code for this application as well as a desktop Python base GUI application is available [here](https://gitlab.com/voxl-public/voxl-cam-manager).  Also, a Dockerized build environment is available [here](https://gitlab.com/voxl-public/apps-proc-cross-compiler-docker), so one could customize as needed.

## Requirements

### Hardware

- Host computer (Ubuntu 18.04 used in this guide)
- VOXL-DK
  - VOXL Core Board (MCCA-M0006)
  - Wi-Fi antennas (MANT-00001)
  - APM Regulator Board and cable (MCCA-M0009 and MCBL-00001 cable)
- VOXL-ACC-PS
  - 5V/6A Power Supply (MPS-00001-1, MPN: WR9QA6000LCP-N(R6B)
- VOXL-ACC-MV
  - Sunny GP161C Stereo Cameras and Flex Cable
  - Sunny MD102A Downward Facing Tracking (DFT) Camera and Flex Cable


### Target Software

- Cameras must be configured wtih voxl-configure-cameras. For more details refer to the [quickstarts](/configure-cameras/).

### Host System

Host computer (Ubuntu 18.04 used in this user guide) with:

- Python 2.7
- Install the following packages

```bash
me@mylaptop:~$ sudo apt-get install python-dev
me@mylaptop:~$ sudo apt-get install python-qt4
```

- Clone the following repo in order to fetch the image streamer python program.

```bash
me@mylaptop:~$ git clone https://gitlab.com/voxl-public/voxl-cam-manager.git
```

## Setup

### Camera Connections

The Machine Vision Accessory Kit (`VOXL-ACC-MV`) comes with the `MD102A` Tracking camera and flex cable and `GP161C` stereo cameras and flex cable.

- Disconnect power
- Connect the `GP161C` cameras and flex cable to J3 of the VOXL board as shown:
  - **NOTE**: when connecting the flex cable to J3, ensure the connectors are parallel when attaching and a solid 'snap' occurs to best ensure a good connection

![camera-stream-0](/images/userguides/camera-stream-0.png)

- Connect the `MD102A` camera and flex cable to J4 of the VOXL board as shown:
  - **NOTE**: again, when connecting the flex cable to J4 this time, ensure the connectors are parallel when attaching and a solid 'snap' occurs to best ensure a good connection

![camera-stream-1](/images/userguides/camera-stream-1.png)


### Target Software Setup

- Connect power to VOXL
- Connect the VOXL board via USB to the host computer
- Make sure the VOXL Software bundle is installed

```bash
yocto:~$ cat /etc/modalai/voxl-software-bundle-version.txt
```

## Usage

## Network Configuration

- Configure the VOXL as an Access Point (or connect it to a network)
  - instructions on how to do this are [here](/wifi-setup/)
  - the default IP address of the device will be `192.168.8.1`
- Connect the host computer to the VOXL's Access Point (default password is 1234567890) or to the same network you connected the VOXL to

## Stream with voxl-image-streamer

### Run Application On Target

```bash
/# voxl-image-streamer -h
usage: voxl-image-streamer -c [CAM_TYPE]... ARGS
   or: voxl-image-streamer -c [CAM_TYPE] -i [IP]... ARGS
-h                print this message.
-c [CAM_TYPE]     0=HIRES, 1=TRACKING, 2=STEREO
-i [IP]           IP address of the VOXL
-p [PORT]         port number to stream on, default 5556
-e [EXP]          set exposure; EXP between 0 and 1, default 0.36
-r [WIDTHxHEIGHT] image widthxheight, default 640x480
-g [GAIN]         set gain; GAIN between 0 and 1, default 0.35
-n [NUM]          stream every n-th image, default 1

Note, this chooses the camera IDs based on teh environment variables
set in /etc/modalai/camera_env.sh
Set up this file before use with the voxl-configure-cameras tool!

The -i ip address argument is optional. If omitted, the TCP stream
server will not start but the camera will still initialize.
Available resolutions are printed when the program starts.
Note, image_viewer.py currently does not show color.
```

- For example, stream the stereo cameras using the following command:

```bash
yocto:~$ voxl-image-streamer -c 2 -n 4 -i 192.168.8.1
```

### Run Application On Host

On the host computer, which is connected to the VOXL's network, run the following:

```bash
me@mylaptop:~$ cd voxl-cam-manager/image_streamer/
me@mylaptop:~$ python image_viewer.py -i 192.168.8.1
```

The stereo camera video stream should display in a GUI.

