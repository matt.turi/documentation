---
layout: default
title: Stream RTSP video using MIPI cameras
parent: Camera and Video Guides
grand_parent: Developing with VOXL
nav_order: 7
permalink: /voxl-rtsp/
---

# Stream RTSP video using MIPI cameras from VOXL (VOXL-RTSP)
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

[voxl-streamer](https://docs.modalai.com/voxl-streamer/) is replacing voxl-rtsp as the framework to access MIPI cameras. voxl-rtsp will be deprecated in the future.

[VOXL-RTSP](https://gitlab.com/voxl-public/voxl-rtsp) is a simple application to stream video from a VOXL image sensor using RTSP and, optionally, store video locally to a MP4 file. Run the video streaming application from the VOXL and view using a RTSP viewer such as VLC or Quicktime player.

## Running VOXL-RTSP

The VOXL's connectors are attached to the following Image Sensor Subsystem ports and have the following colloquial names.

| Port  | Image Sensor ID   | Colloquial Name               |
|------ |----------------   |------------------------------ |
| J2    | 0                 | Hi-res Image Sensor Port      |
| J3    | 1                 | Stereo Image Sensor Port      |
| J4    | 2                 | Tracking Image Sensor Port    |


Run the streaming server on target, using the `-c` option with the specified Image Sensor ID to choose which image sensor to stream video from.

```bash
$ voxl-rtsp -c 0 # Stream video from Hi-res Image Sensor
Simple RTSP starting
Camera number: 0
Connected
Started camera 0
Created session
Video track created
Updated camera parameters
AE Mode: 1, Exposure: 15814, Gain: 100, AWB Mode: 1
rtsp://DEVICE_IP:PORT/live
Started session
Ctrl-c or 'kill 3038' to end
```

Some parameters to try:

Resolution: ```-s 3840x2160```

Bitrate: ```-b 10000000```


## Viewing Video Stream


### RTSP Streaming to VLC or Quicktime

Open streaming server's RTSP URL in VLC or QuickTime player: `rtsp://DEVICE_IP:PORT/live`

- Connect host system to VOXL's WiFi

- On the host system, run the VLC software

- Go to `Media` > `Open Network Stream`

- Enter RTSP URL, default: `rtsp://192.168.8.1:8900/live`

- Click Play to view the video stream

### RTSP Streaming to QGroundControl

- The first step to streaming video to QGroundControl is connecting the VOXL system to QGroundControl through an IP connection. Instructions on establishing a connection to QGroundControl can be found [here](https://docs.modalai.com/qgc-wifi/).

- Next, in QGroundControl, press the purple QGC logo in the top left corner in order to Access the `Application Settings` menu.

- Under the `General` tab, scroll down until you find the `Video` section.

- Under the `Video Source` dropdown, choose `RTSP Video Stream`

- In the `RTSP URL` field. enter the RTSP URL, default: `rtsp://192.168.8.1:8900/live`

- You will now be able to view the video stream under QGroundControl's `Fly` view.

## Accessing Raw Frames

The voxl-rtsp `-m` option allows access to the raw frames from the MIPI cameras. This option creates a FIFO special file, a named pipe, that can be opened by many different types of processses for reading or writing.
