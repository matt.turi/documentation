---
layout: default
title: Camera Server
parent: Camera and Video Guides
grand_parent: Developing with VOXL
nav_order: 10
permalink: /camera-server/
---

# Camera Server
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

voxl-camera-server is built around the Modal Pipe Architecture (using [libmodal_pipe](https://gitlab.com/voxl-public/libmodal_pipe)) to serve up MIPI cameras to mulitple clients. On top of this architecture will be client applications for ROS, RTSP, Docker, and many more.

voxl-camera-server is now in beta. Most of the documentation is currently inline with [the code](https://gitlab.com/voxl-public/voxl-camera-server).

## Stream Camera Data Over RTSP

The [voxl-streamer](https://docs.modalai.com/voxl-streamer/) application can be used to stream video from voxl-camera-server.
