---
layout: default
title: Build for GPU (OpenCL)
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 4
permalink: /build-for-gpu-opencl/
---

# Build for GPU (OpenCL)

## Overview

VOXL contains an embedded [Adreno 530](https://en.wikipedia.org/wiki/Adreno) GPU with 256 ALUs. This GPU is exposed through both OpenCL 1.1 and OpenGLES 3.1. The GPU can be exploited for significant algorithmic acceleration for use cases like computer vision and deep learning.

## Examples


Examples of how to write code for the Qualcomm® Adreno™ 530 GPU

| Name | Description | Link |
| --- | --- | --- |
| Hello CL | Simple application showing how to interact with the GPU using OpenCL | [GitLab](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellocl) |
| TensorFlow Lite | Use TensorFlow Lite with OpenCL on VOXL | [Docs](https://docs.modalai.com/tflite/) |
| modalai-opencl-dev_1.0-1.deb | Debian to install on an ARMv8 Ubuntu Docker running on VOXL Code	| [Download](https://developer.modalai.com/asset/eula-download/39) |
| OpenCV Docker ARMv8 with OpenCL | | [Code](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-opencv-opencl) |
