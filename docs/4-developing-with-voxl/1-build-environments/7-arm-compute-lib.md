---
layout: default
title: ARM Compute Lib on VOXL
parent: Build Environments
grand_parent: Developing with VOXL
nav_order: 7
permalink: /arm-compute-lib/
---

# How to use ARM Compute Lib on VOXL


## Examples

ARM Compute Libary example for VOXL [Download IPK](https://storage.googleapis.com/modalai_public/modal_packages/latest/voxl-armcl_19.08.ipk), [Gitlab](https://gitlab.com/voxl-public/voxl-armcl).