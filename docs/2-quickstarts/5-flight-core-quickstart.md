---
layout: default
title: Flight Core Quickstart
nav_order: 5
parent: Quickstarts
has_children: true
permalink: /flight-core-quickstart/
has_toc: false
---

# Flight Core Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![fc-dk](/images/quickstart/flight-core/fc-dk.jpg)

## Guided Setup

- [Start Here](/flight-core-getting-started/)

## Helpful Links

- [Flight Core Support Forum](https://forum.modalai.com/category/10/flight-core)
- [User Guide](/flight-core-getting-started/)
- [Data Sheet](/flight-core-datasheet/)
- [PWM ESC Calibration](/flight-core-pwm-esc-calibration/)

## Connectors - Top

![Flight Core Top](../../../images/datasheet/fc-overlay-top-144-dpi.png)

| Connector | Summary | |
| --- | --- | --- |
| J1  | VOXL Communications Interface Connector (TELEM2) | |
| J2  | Programming and Debug Connector | |
| J3  | USB Connector | |
| J4  | Spare UART/UART ESC (TELEM3) | |
| J5  | Telemetry Connector (TELEM1) | |
| J6  | VOXL-Power Management Input / Expansion | |
| J7  |  8-Channel PWM Output Connector | |
| J8  | CAN Bus Connector | |
| J9  | PPM RC In | |
| J10  | External GPS & Magnetometer Connector | |
| J12  | RC Input, Spektrum/SBus/UART Connector | |
| J13  | I2C Display (Spare Sensor Connector) / Safety Button Input | |

## Connectors - Bottom

![Flight Core Core Bottom](../../../images/datasheet/fc-overlay-bot-144-dpi.png)
