---
layout: default
title: LTE Modem Quickstart
nav_order: 7
parent: Quickstarts
has_children: true
permalink: /lte-modem-quickstart/
has_toc: false
---

# LTE Modem Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![lte-v2](/images/quickstart/lte/lte-v2-hero.jpg)

## VOXL Add-On Version

### Helpful Links

- [LTE Modem Support Forum](https://forum.modalai.com/category/12/lte-modems)
- [User Guide, LTE v2](/lte-v2-modem-manual/)
- [Data Sheet, LTE v2](/lte-modem-and-usb-add-on-v2-datasheet/)
- [User Guide, LTE v1](/lte-v1-modem-manual/)
- [Data Sheet, LTE v1](/lte-modem-and-usb-add-on-datasheet/)

### Connectors - Top

![m0030-1-top.png](../../images/datasheet/voxl-lteh/m0030-1-top.png)

### Connectors - Bottom

![m0030-1-bottom.png](../../images/datasheet/voxl-lteh/m0030-1-bottom.png)

## Standalone Version

### Helpful Links

- [LTE Modem Support Forum](https://forum.modalai.com/category/12/lte-modems)
- [User Guide, LTE v2](/lte-v2-modem-manual/)
- [Data Sheet, LTE v2](/lte-modem-v2-dongle-datasheet/)

### Connectors - Top

![m0030-2-top.png](../../images/datasheet/voxl-lteh/m0030-2-top.png)

### Connectors - Bottom

![m0030-2-bottom.png](../../images/datasheet/voxl-lteh/m0030-2-bottom.png)
