---
layout: default
title: Setup ADB
parent: VOXL Quickstarts
parent: VOXL Quickstart
grand_parent: Quickstarts
nav_order: 3
permalink: /setup-adb/
---

# Setup ADB
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The following describes how to setup the Android Debug Bridge (ADB) tool on a host computer.  This tool is used to facilitate communications and file transfers between the VOXL and the host computer.


## Host Computer Setup

Install the Android Debug Bridge (ADB):

```bash
me@mylaptop:~$ sudo apt install android-tools-adb android-tools-fastboot
```

*ModalAI Top Tip*: to run ADB without root on the host PC, create a file called `/etc/udev/rules.d/51-android.rules` containing this udev rule on the host computer:

```bash
me@mylaptop:~$ sudo su
root@mylaptop:/home/me# echo 'SUBSYSTEM=="usb", ATTRS{idVendor}=="05c6", ATTRS{idProduct}=="901d", MODE="0660", GROUP="plugdev", SYMLINK+="voxl%n"' > /etc/udev/rules.d/51-android.rules
root@mylaptop:/home/me# udevadm control --reload-rules && udevadm trigger
```


## Common ADB Commands

Listed here for convenience are some common ADB commands.


### Check for Devices

From the host, you can check for connected devices using `adb devices`

```bash
me@mylaptop:~$ adb devices
List of devices attached
73a05d48	device
```


### Access the Shell

You can gain access to VOXL's shell from the host computer using `adb shell`

```bash
me@mylaptop:~$ adb shell
```

This will start an sh shell. Generally you want to use a bash shell instead, so we recommend starting bash right away. Running `adb shell`  followed by starting 'bash' will look like this:

```bash
me@mylaptop:~$ adb shell
/ # bash
yocto:/#
```

You should see the 'yocto' prompt after starting bash. To confirm, you can also look at the `SHELL` variable:

```bash
yocto:/# echo $SHELL
/bin/bash
```


### Exit out of adb shell

Just use the `exit` command to exit. If you started a bash shell inside the sh shell as described above, you will need to run exit twice to exit out of each. The process to start a shell, start bash, and exit again looks like this:

```bash
me@mylaptop:~$ adb shell
/ # bash
yocto:/# echo "I am inside VOXL!"
I am inside VOXL!
yocto:/# exit
exit
/ # exit
me@mylaptop:~$
```


### Run Something Once with `adb shell <command>`

If you don't want to start an interactive shell, but just want to run one command on VOXL, you can pass the command directly to `adb shell`.

```bash
me@mylaptop:~$ adb shell echo "I'm inside VOXL"
I'm inside VOXL
me@mylaptop:~$
```


### Wait for Device

A useful command for creating scripts for host-to-target communications is this one that sits and waits until a device is connected:

```bash
me@mylaptop:~$  adb wait-for-device
```


### Copying Files

You can copy files to/from VOXL (similar to `scp`) using the `adb push` and `adb pull` commands.

For example, to copy `/temp/test.dat` to the VOXL at `/`:

```bash
me@mylaptop:~$ adb push /tmp/test.dat /
/tmp/test.dat: 1 file pushed.
```

To copy it back from the VOXL at `/test.dat` to `/temp`

```bash
me@mylaptop:~$ adb pull /test.dat /tmp
/test.dat: 1 file pulled
```

### Reboot VOXL

You can reboot VOXL with a single adb command `adb reboot`. This is often followed by `adb-wait-for-device` which returns when VOXL has finished rebooting.

```bash
me@mylaptop:~$  adb reboot
```


## Next Steps

Let's [flash the system image](/flash-system-image/).

