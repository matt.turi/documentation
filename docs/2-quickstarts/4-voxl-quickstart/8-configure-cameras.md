---
layout: default
title: Configure Cameras
parent: VOXL Quickstart
grand_parent: Quickstarts
nav_order: 8
permalink: /configure-cameras/
---

# Configure Cameras


When setting up VOXL for the first time or when changing camera configurations, you will need to configure VOXL's software to make it aware of the physical camera configuration. This can easily be done by running the `voxl-configure-cameras` script that's included with the voxl-utils package.

```bash
me@mylaptop:~$ adb shell voxl-configure-cameras
```

By default, this will prompt you for the configuration you are using from the list below. If you like, you can skip the prompt by providing the configuration ID as an argument:

```bash
me@mylaptop:~$ adb shell voxl-configure-cameras 1
```

This script will set up the configuration files for ROS, SNAV, and the ModalAI Vision Lib as described below. It may also be called by the configuration scripts for other packages such as the ModalAI Vision Lib. For this reason it is installed as part of the voxl-utils package from the factory.

For more details on each configuration, see the [Camera Connections page](/camera-connections/).


## Next Steps

Optionally test the cameras in one of the ways described in the [Camera and Video Guides](/camera-video-guides/).

After completing the remainder of the Quickstart Guides, optionally calibrate the cameras using the instructions on the [camera calibration page](/calibrate-cameras/).

Next [set up ROS on your host-pc](/setup-ros-on-host-pc/).

