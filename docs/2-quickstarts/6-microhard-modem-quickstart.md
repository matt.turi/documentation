---
layout: default
title: Microhard Modem Quickstart
nav_order: 6
parent: Quickstarts
has_children: true
permalink: /microhard-modem-quickstart/
has_toc: false
---

# Microhard Modem Quickstart
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

![michrohard](/images/quickstart/microhard/microhard.jpg)

## VOXL Add-On Version

### Helpful Links

- [Microhard Modem Support Forum](https://forum.modalai.com/category/11/microhard-modems)
- [User Guide](/microhard-add-on-manual/)
- [Datasheet](/microhard-add-on-datasheet/)

### Connectors - Top

![voxl-acc-mh-2-top.png](/images/datasheet/microhard/voxl-acc-mh-2-top.png)

### Connectors - Bottom

![voxl-acc-mh-2-bottom.png](/images/datasheet/microhard/voxl-acc-mh-2-bottom.png)

## Standalone Version

### Helpful Links

- [Microhard Modem Support Forum](https://forum.modalai.com/category/11/microhard-modems)
- [User Guide](/microhard-usb-carrier/)
- [Datasheet](/microhard-usb-carrier/)

### Connectors - Top

![ma-sa-top.png](/images/datasheet/microhard/mh-sa-2-top.png)

### Connectors - Bottom

![ma-sa-bottom.png](/images/datasheet/microhard/mh-sa-2-bottom.png)
