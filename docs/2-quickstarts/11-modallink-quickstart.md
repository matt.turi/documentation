---
layout: default
title: Modal-Link Quickstart
nav_order: 11
parent: Quickstarts
has_children: true
permalink: /modallink-quickstart/
has_toc: false
---

# Overview 
Modal-Link is an LTE basestation networking devices on the DoD Band 3 frequency. This quickstart guide will help you set-up and deploy a ModalLink network connecting drones, cellphones, tablets or other supported devices.

<img src="/images/quickstart/modallink/ModalLink.jpg" height="200">

[//]: # "TO DO: Need to add photo of Modal-Link drone, possibly with callouts"
[//]: # "How to set-up Modal-Link antennas, etc"
[//]: # "Need updated Modal-Link photos with new antenna set-up"
[//]: # "Need to send prod team updated components lists."
[//]: # "Need LED picture."

## Components
- ModalLink
  - 3 x Antennas
  - Ethernet cable

`Caution:` When in operation the top metal portion of the Modal-Link enclosure can get very hot. Please use caution when handling in the field.
- Modal-AI SIM Cards
  - SIM Cards are labeled with a number xxx (e.g., 200) and will set their device with an IP address 172.16.0.xxx (e.g., 172.16.0.40).
- Some kits will include ModalLink m500 drone w/ v2 Modem installed and pre-configured.

## Ground Station Set-Up
The following image shows a common ModalLink configuration connected to a host computer running QGrounControl for sUAS connectivity:

<img src="/images/quickstart/modallink/ModalLink_config.jpg" height="200">

Power the femtocell using a 13-25v source. You can connect a 4S-6S LiPo battery pack with XT60 connector.

`Note:` ModalLink can be used with or without a host. If you choose to configure the system without a host, ModalLink must be connected to a network switch or similar to enable networking. If you only have one host device, you can connect it directly to the femtocell without using a network switch. If you have more than one host device, you can connect them all to a network switch using the ModalLink subnet `192.168.88.xxx`.

### Device IP Addresses
Each device connecting to ModalLink wirelessly requires a SIM card. ModalLink supported VOXL modem carrier boards and the tested devices listed below are supported. Each SIMcard is associated with an IP address, eg. SIM card 50 is associated with 172.16.0.50.

### IPv4 IP Forwarding
Enable IP forwarding to allow devices to connect over multiple network interfaces. In Ubuntu, for instance, update `/etc/sysctl.conf` and set `net.ipv4.ip_forward=1`:
  ```bash
  # In file /etc/sysctl.conf
  net.ipv4.ip_forward=1
  ```
Then run the following verify the output:
  ```bash
  sudo sysctl -p
  ```
Output:
  ```bash
  net.ipv4.ip_forward = 1
  ```

### Host Ethernet Interface
Connect the Ethernet cable to your host computer. In order to connect to devices on the ModalLink network, use the following host computer settings:
- IP Address: 192.168.88.201 (any IP on this subnet, except `192.168.88.100` or `192.168.88.1`)
- Netmask: 255.255.255.0
- Gateway: 192.168.88.100

If you would like your host machine to connect to an additional network, such as the internet, you can configure your internet interface first as the default route then manually add routes on your ModalLink interface for the `172.16.0.XXX` device IP addresses with 192.168.88.100 as the gateway (depending on your chosen operating system). 

In Ubuntu, you can use the connection manager to set this configuration:

<img src="/images/quickstart/modallink/ubuntu_connection_mgr_device_routes.png" height="200">

### QGroundControl
If you're using a pre-configured drone shipped with Modal-Link, ensure your Ground Control computer is set with the recommended IP listed above. 

If you like to reconfigure IPs or need to set-up QGroundControl on another platform use these [instructions](https://docs.modalai.com/qgc-wifi/#ground-control-station-ip).

### Modal-Link Status
When Modal-Link set-up properly and is Ready, all three green LEDS on the side are solid. Once a device is connected the lower LED flashes.

### Antenna Configuration
To maximize Modal-Link reception and transmission, the following shows the ideal antenna orientation:

<img src="/images/quickstart/modallink/modallink_ants_config.jpg" height="200">

## Drone or Device Setup
### VOXL Modem v2
The VOXL World Modem is a band 3 compatible modem bundled with VOXL. When the modem is properly connected to the network, an IP address is assigned to VOXL defined by the installed SIM card. In most shipments, the system comes pre-configured for Modal-Link. For more information on configuration and installing a SIM card go [here](https://docs.modalai.com/lte-v2-modem-manual/).

### Modal-Link m500 Drone
The Modal-Link m500 drone comes preconfigured with VOXL Modem v2. The drone has three antennas installed: amplified transmit, receive, and diversity. 

<img src="/images/quickstart/modallink/modallink_drone_front.jpg" height="150">
<img src="/images/quickstart/modallink/modallink_drone_back.jpg" height="150">

### Cellphones and Tablets
Factory un-locked International cellphones and tablets are supported on ModalLink. The following devices have been tested:
- Samsung Galaxy s9 Cellphone (SM-G960U1)
- Samsung Galaxy Tab A Tablet (SM-T295)
- Samsung Galaxy Tab A Tablet 2020 (SM-T307U)
- iPad Pro Tablet (MPHG2LL/A)
### APN Configuration
With the SIM card properly installed, configure the APN in your device settings by adding the following:
```
Name: modalai
APN: modalai
APN type: default, mms, supl, hipri, fota, ims, cbs
```
When these settings are properly configured, you should see an LTE or 4G logo next to the signal strength indicator on the status bar. In addition, you can confirm configuration in device settings under status,displaying Modal-AI as the network operator and an IP address on the `172.16.0.XXX` subnet. 
