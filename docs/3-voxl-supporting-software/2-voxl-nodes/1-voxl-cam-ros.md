---
layout: default
title: VOXL Cam ROS
parent: ROS Nodes
grand_parent: VOXL Supporting Software
nav_order: 1
permalink: /voxl-cam-ros/
---

# VOXL Cam ROS

ROS Node for using MIPI cameras on the VOXL platform.

VOXL Cam ROS is included in the [VOXL Nodes package](/voxl-nodes/) is included in the [VOXL Software Bundle](/install-software-bundles/)

## Prerequisites

To play with the ROS nodes, first follow the quickstart guides to [setup ROS on host PC](/setup-ros-on-host-pc/) and [setup ROS on VOXL](/setup-ros-on-voxl/).

You must also [configure the cameras](/configure-cameras/) before running the ROS node.

## Usage

VOXL Cam ROS includes the following six launch files:

 * hires.launch
 * hires_color.launch
 * stereo.launch
 * stereo_merged.launch
 * tracking.launch
 * tracking_slave.launch


Each launch file has the following variables settable as command line arguments to roslaunch. Obviously, the defaults for each file are different and appropriate to each camera type.

```xml
<!-- cmd line arguments to launch file -->
<arg name="cam_name" default="tracking"/> <!-- sets the topic namespace -->
<arg name="cam_id" default="$(env TRACKING_CAM_ID)"/> <!-- configure with voxl-configure-cameras -->
<arg name="skip_n_frames" default="0"/>
<arg name="frame_rate" default="15" />
<arg name="width" default="640"/>
<arg name="height" default="480"/>
<arg name="frame_id" default="tracking_frame"/> <!-- transform frame used by TF2 -->
<arg name="is_cam_master" default="true"/>
```

### Cam Name and cam ID

The camera name parameter will set the topic name that is published and has nothing to do with selecting which camera is being opened. The exact camera that is opened is specified by the cam_id parameter and defaults to reading the environment variables TRACKING_CAM_ID, STEREO_CAM_ID, and HIRES_CAM_ID which are set with the [voxl-configure-cameras script](/configure-cameras/).

### Frame Rate and skip_n_frames

The frame rate for tracking and stereo cameras can only be set to 15, 30, and 60 hz. This is all the camera module itself supports. If you need slower frame rates, you can specify the skip_n_frames parameter to subsample.

### Width and Height

The tracking and stereo cameras only support 640x480 VGA resolution. the hires camera specifies a variety of resolutions which are printed out by the voxl_cam_ros node when it starts.

### Cam Master

This ROS node will fail to start if voxl-vision-px4 or any other process is using the camera in the background. You can get around this by either stopping the offending background process, or starting the ROS node in slave mode where the background process remains in control of the camera but ROS is allowed to read the images.

```bash
# on VOXL
yocto:~# roslaunch voxl_cam_ros stereo.launch is_cam_master:=false
```

Since the tracking camera is almost always being used in the background for VIO, we provide a shortcut launch file called tracking_slave.launch which launches the tracking camera in slave mode.

```bash
# on VOXL
yocto:~# roslaunch voxl_cam_ros tracking_slave.launch
```

### Auto-exposure and Auto-white-balance
By default voxl-cam-ros has auto-exposure disabled. To enable, you should comment out these two lines [here](https://gitlab.com/voxl-public/voxl-cam-manager/-/blob/master/src/SnapdragonCameraManager.cpp#L188)

```
params_.setManualExposure(exposure_setting_);
params_.setManualGain(gain_setting_);
```

The default setting is auto exposure. The auto white balance should be enabled by default.

## Known Issue Running ROS Through SSH

If you try to use ROS commands while SSH'd to VOXL and see errors such as this:

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the Ubuntu terminal emulator. The solution is to use another terminal emulator such as xterm instead of Ubuntu's default terminal.

More information here: https://gitlab.com/voxl-public/documentation/issues/4


