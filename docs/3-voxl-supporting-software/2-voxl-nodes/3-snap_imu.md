---
layout: default
title: Snap IMU
parent: ROS Nodes
grand_parent: VOXL Supporting Software
nav_order: 3
permalink: /snap-imu/
---

# Snap IMU
{: .no_toc }



## Prerequisites

IMU app must be running in the background, this is installed as part of the VOXL software bundle. It is a background service that handles the IMU and lets multiple processes consume the data simultaneously such a the modalai vision lib and the snap_imu ROS node. It samples at 500hz.

``` bash
yocto:/# systemctl status imu_app
● imu_app.service - IMU application
   Loaded: loaded (/usr/bin/imu_app; enabled; vendor preset: enabled)
   Active: active (running) since Thu 1970-01-01 00:00:11 UTC; 50 years 1 months ago
```

## Usage

snap_imu only has one launch file (imu.launch) which has one user-configurable parameter which is the sample rate. The maximum sample rate is 500hz and the default is 100hz.

```bash
yocto:/# roslaunch snap_imu imu.launch rate:=500
```

```bash
yocto:/# rostopic hz /imu
subscribed to [/imu]
average rate: 500.458
    min: 0.000s max: 0.005s std dev: 0.00065s window: 496


```bash
yocto:/# rostopic echo -n 1 /imu
header:
  seq: 35687
  stamp:
    secs: 1581715425
    nsecs:  24289277
  frame_id: imu
orientation:
  x: 0.0
  y: 0.0
  z: 0.0
  w: 0.0
orientation_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
angular_velocity:
  x: -0.00213052891195
  y: -0.00639158673584
  z: 0.00532632227987
angular_velocity_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
linear_acceleration:
  x: 0.119669213891
  y: -1.20626568794
  z: 9.81287574768
linear_acceleration_covariance: [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
```

## imu array data

If you run the ros node with slower than 500hz sample rate, the /imu topic will be published at the requested rate but full 500hz imu data is still available through the /imu_raw_array topic. For example, if you specify a rate of 100 then the /imu_raw_array topic will contain 5 samples in each publication.


## Configuration IMU filter

The imu_app server configures the IMU's internal low pass filter on boot and can be configured in its service file starting from voxl-software-bundle 0.0.4.

```bash
yocto:/# vi /etc/systemd/system/imu_app.service
```

```
modify the -l argument on this line:
ExecStart=/usr/bin/imu_app -s 2 -p 1 -t 2 -l 92
```

The options are 184, 92, 41, 20, 10, and 5 hz and configure the filter internally in the IMU;

```bash
systemctl restart imu_app
```