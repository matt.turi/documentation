---
layout: default
title: VOXL Utilities
parent: VOXL Supporting Software
nav_order: 3
permalink: /voxl-utils/
---

# VOXL Utilities
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}


## Overview

VOXL Utilities (voxl-utils.ipk) is a collection of utilities for setup, configuration and testing tasks and is included in the [VOXL Software Bundle](/install-software-bundles/).

While using the shell on VOXL, typing in `voxl` and hitting the `TAB` key will list all installed and available VOXL CLI programs. Many of these are provided by VOXL Utilities.

```bash
/# voxl
voxl-configure-cameras     voxl-perfmon
voxl-configure-modalai-vl  voxl-test-vision-lib
voxl-configure-snav        voxl-uart-loopback
voxl-configure-vision-px4  voxl-version
voxl-env                   voxl-vision-px4
voxl-hwscan                voxl-wifi
voxl-image-streamer        voxl_imu_test
```


## voxl-wifi

This utility is used to setup the Wi-Fi mode.  The VOXL can be setup in `station` mode, in which it connects to an Access Point, or it can be setup in `softap` mode, where it itself acts as an Access Point, allowing clients to connect to it.

For details on how to use voxl-wifi, go to the [WiFi Setup page](/wifi-setup/).



## voxl-env

The `voxl-env` utility can be used to inspect ROS and VOXL-specific bash environment variables.

```bash
/# voxl-env show
ROS_MASTER_URI=http://localhost:11311/
ROS_IP=192.168.1.56
CAM_CONFIG_ID=1
HIRES_CAM_ID=-1
TRACKING_CAM_ID=0
STEREO_CAM_ID=1
TOF_CAM_ID=-1
```


## voxl-hwscan (Work in Progress)

The `voxl-hwscan` utility runs various hardware tests and queries on target. Right now it only shows the mac address, more to come later!

```bash
/# voxl-hwscan mac
48:5F:99:9D:40:F7
```


## voxl-perfmon

The `voxl-perfmon` tool can be used to run the performance profiler. The tool computes normalized CPU and GPU use across frequency scaling. It should run on any Snapdragon platform with Linux support, but this has only been tested on Snapdragon 820 Yocto. Source code [here](https://gitlab.com/voxl-public/voxl-utils/-/blob/master/bin/voxl-perfmon).

```bash
/# voxl-perfmon

Name   Freq (MHz) Temp (C) Util (%)
-----------------------------------
cpu0        307.2     25.8     1.56
cpu1        307.2     25.8     0.99
cpu2       1324.8     28.0     1.03
cpu3       1324.8     28.3     0.62
-----------------------------------
Total                          1.05
-----------------------------------
POP memory temperature:   25.4
GPU temperature:          24.1
Maximum temperature:      29.9
-----------------------------------
Total memory (MB): 3771
Used memory  (MB): 446
Free memory  (MB): 3325
```

Here are some more interesting Commands for CPU profiling:

- Check POP Memory Temperature: ```cat /sys/devices/virtual/thermal/thermal_zone1/temp```
- Set CPU1 Offline: ```echo "0" > /sys/devices/system/cpu/cpu1/online```
- View offline CPUs: ```cat  /sys/devices/system/cpu/offline```
- View online CPUs: ```cat  /sys/devices/system/cpu/online```
- Set CPU frequency scaling governor: ```echo "powersave"> /sys/devices/system/cpu/cpu3/cpufreq/scaling_governor```
- Stress the CPU for thermal testing: ```stress -c 4```



## voxl-version

The `voxl-version` command displays the various version information.

```bash
me@mylaptop:~$ adb shell voxl-version
--------------------------------------------------------------------------------
system-image:    ModalAI 1.10.0 BUILDER: ekatzfey BUILD_TIME: 2019-11-11_23:05
kernel:          #1 SMP PREEMPT Mon Nov 11 22:28:13 UTC 2019 3.18.71-perf
factory-bundle:  0.0.2
sw-bundle:       0.0.2
--------------------------------------------------------------------------------
architecture:    aarch64
processor:       apq8096
os:              GNU/Linux
--------------------------------------------------------------------------------
voxl-utils:
Package: voxl-utils
Version: 0.4.3
Status: install user installed
Architecture: aarch64
Installed-Time: 14

--------------------------------------------------------------------------------
```




## voxl-configure-cameras

Configures camera IDs for [ROS](/voxl-nodes/), [VOXL Camera Manager](/voxl-cam-manager/),(ModalAI Vision Library)[/modalai-vision-lib/], and other utilities that use cameras.

See the [Camera Configuration page](/configure-cameras/) for more info.

```bash
/# voxl-configure-cameras -h
General Usage:
voxl-configure-cameras <configuration-id>

If no configuration-id is given as an argument, the user will be prompted.

show this help message:
voxl-configure-cameras media_scripts -h

available camera configurations are as follows:
1 Tracking + Stereo (default)
2 Tracking Only
3 Hires + Stereo + Tracking
4 Hires + Tracking
5 TOF + Tracking
6 Hires + TOF + Tracking
7 TOF + Stereo + Tracking
```
