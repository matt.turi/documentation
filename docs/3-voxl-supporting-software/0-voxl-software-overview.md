---
layout: default
title: VOXL Software Overview
parent: VOXL Supporting Software
nav_order: 0
has_children: false
permalink: /voxl-software-overview/
---

# VOXL Software Functional Overview
{: .no_toc }

VOXL is a robotics development platform with hundreds of different features. The goal of this functional overview is to describe the most popular features by function, and provide links to the appropriate resources.

## Function Matrix

| Function          | Description | Binary | Source Code | Usage Instructions | 
| ---              | ---         | ---    | ---         | ---                |
| Flight Control - PX4 | PX4 Open Source Flight controller| | [Firmware Guide](https://docs.modalai.com/flight-core-firmware/| [Flying with VOXL](https://docs.modalai.com/flying-with-voxl/) | 
| Flight Control - Snapdragon Navigator | 1.4.1 (snav-modalai_1.4.1_8x96.ipk) | [Developer Website](https://developer.modalai.com/asset) | Source N/A | [Snapdragon Navigator Usage](https://docs.modalai.com/snapdragon-navigator/) | 
| ROS | ROS setup instruction and hardware access nodes | | [voxl-nodes](https://gitlab.com/voxl-public/ros/voxl-nodes) | [Setup ROS on VOXL](https://docs.modalai.com/setup-ros-on-voxl/) <br>[VOXL Nodes Guide](https://docs.modalai.com/voxl-nodes/) | 
| Camera Calibration for Computer Vision | A ROS environment to calibrate stereo and fisheye cameras | | [Gitlab](https://gitlab.com/voxl-public/voxl-camera-calibration) | [Guide](https://docs.modalai.com/calibrate-cameras/) |
| Autonomous Flight | PX4 Avoidance and GPS-denied Navigation | | [VOXL Vision PX4](https://docs.modalai.com/voxl-vision-px4/) | [Guide](https://docs.modalai.com/voxl-vision-px4/) | 
| Stream Video - UVC Camera |  | | | [Guide](https://docs.modalai.com/uvc-streaming/) | 
| Stream Video - USB Camera (like DSLR) |  | | [Source](https://gitlab.com/voxl-public/utilities/voxl-libgphoto2) | [Guide](https://docs.modalai.com/voxl-libgphoto2/) | 
| Stream Video - MIPI Camera |  |  | [hellocamera Example](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellocamera) [hellovideo Example](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellovideo) <br>  [API](https://source.codeaurora.org/quic/la/platform/hardware/qcom/camera/tree/libcamera?h=LNX.LER.1.2) | [voxl-streamer Guide](https://docs.modalai.com/voxl-streamer/) <br> [Usage Guide](https://docs.modalai.com/camera-usage/)| 
| Video Encoding - MIPI Camera |  |  | [hellovideo Example](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellovideo) | [voxl-streamer Guide](https://docs.modalai.com/voxl-streamer/) | 
| Connect to LTE |  | `voxl-modem` | [voxl-modem](https://gitlab.com/voxl-public/voxl-modem) | [LTE Instructions](https://docs.modalai.com/connect-to-lte/) | 
| Connect to Microhard |  | `voxl-modem` | [voxl-modem](https://gitlab.com/voxl-public/voxl-modem) | [Microhard Instructions](https://docs.modalai.com/microhard-add-on-manual/) | 
| Connect to Wi-Fi |  | `voxl-wifi` | [Source](https://gitlab.com/voxl-public/utilities/voxl-utils/-/blob/master/bin/voxl-wifi) | [Wi-Fi Instructions](https://docs.modalai.com/wifi-setup/) | 
 

## Developer Tool Matrix

| Function          | Description | Binary | Source Code | Usage Instructions | 
| ---              | ---         | ---    | ---         | ---                | 
| Programming for CPU | VOXL Emulator for development and cross-compile Dockers | | [Hello World](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/helloworld)| [Instructions](https://gitlab.com/voxl-public/voxl-docker) | 
| Programming for GPU | Use on-board GPU for hardware acceleration of algorithms | | [OpenCL Example](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellocl) | | 
| Programming for sDSP | Sensors DSP | Current: [Hexagon SDK v3.0](https://developer.qualcomm.com/download/hexagon/hexagon-sdk-v3-linux.bin) {::nomarkdown} <br> {:/} Planned: v3.4.2  | | [Instructions](https://gitlab.com/voxl-public/voxl-docker)| 
| Programming for aDSP | Apps DSP | Current: [Hexagon SDK v3.1](https://developer.qualcomm.com/download/hexagon/hexagon-sdk-v3-1-linux.bin) {::nomarkdown} <br> {:/} Planned: v3.4.2 | | [Instructions](https://gitlab.com/voxl-public/voxl-docker)| 
| Programming using Python | Scripts to compile and package Python 3.6.9 for the VOXL platform | | [Gitlab](https://gitlab.com/voxl-public/voxl-python-3.6.9) | | |
| Programming Computer Vision - Deep Learning (AI) | Example using OpenCV and ARM-CL to perform deep learning tasks on live video feed | | [hellovideo example](https://gitlab.com/voxl-public/apps-proc-examples/tree/master/hellovideo) | | 
| Programming Computer Vision - ARM Compute Lib | opencv-3-4-6 | | [VOXL ARMCL](https://gitlab.com/voxl-public/voxl-armcl) <br> [AlexNet Example](https://gitlab.com/voxl-public/apps-proc-examples/blob/master/hellovideo/src/graph_alexnet.cpp) | | 
| Programming Computer Vision - OpenCV | opencv-3-4-6 | | | | 
| Programming Computer Vision - Qualcomm MV SDK |  | | | | 
| Linux Kernel |  | | | | 

