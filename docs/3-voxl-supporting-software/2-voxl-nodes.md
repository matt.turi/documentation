---
layout: default
title: ROS Nodes
parent: VOXL Supporting Software
nav_order: 1
has_children: true
permalink: /voxl-nodes/
---

# ROS Nodes
{: .no_toc }

This section contains instructions on how to use provided ROS nodes for VOXL.


To play with the ROS nodes, first follow the quickstart guides to [setup ROS on host PC](/setup-ros-on-host-pc/) and [setup ROS on VOXL](/setup-ros-on-voxl/).

[voxl-nodes](https://gitlab.com/voxl-public/ros/voxl-nodes) source code.


## Known Issue Running ROS Through SSH

If you try to use ROS commands while SSH'd to VOXL and see errors such as this:

```bash
load_parameters: unable to set parameters (last param was [/rosdistro=indigo
]): <Fault 1: "<class 'xml.parsers.expat.ExpatError'>:not well-formed (invalid token): line 13, column 15">
```

The cause is a known issue with the Ubuntu terminal emulator. The solution is to use another terminal emulator such as xterm instead of Ubuntu's default terminal.

More information here: https://gitlab.com/voxl-public/documentation/issues/4



# Nodes:
The VOXL Nodes package (voxl-nodes.ipk) is included in the [VOXL Software Bundle](/install-software-bundles/) and includes the following ROS nodes: