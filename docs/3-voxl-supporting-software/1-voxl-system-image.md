---
layout: default
title: VOXL System Image
parent: VOXL Supporting Software
nav_order: 1
has_children: false
permalink: /voxl-system-image/
---

# VOXL System Image

This page describes the Yocto System Image that runs on VOXL. In the [VOXL Quickstarts](/voxl-quickstarts/), you can find details on [how to flash the VOXL system image](/flash-system-image/).

Yocto Jethro Linux kernel 3.18 [Source](https://gitlab.com/voxl-public/voxl-build), [Build Instructions](https://gitlab.com/voxl-public/voxl-build/blob/master/README.md)

Yocto Jethro Linux Userspace: [Source to add new userspace packages](https://gitlab.com/voxl-public/poky/tree/modalai-sd820-jethro).

Many new packages can be added using bitbake. We have a bitbake project that enables building new IPK packages for install on target here: [Source](https://gitlab.com/voxl-public/poky/tree/modalai-sd820-jethro)

## Download Location

You can [download the system image](https://developer.modalai.com/asset) from our protected downloads page.

## Changelog

| VOXL Release   | Release Notes                                                                                                         |
|:---------------|:----------------------------------------------------------------------------------------------------------------------|
| modalai-2-5-3 (beta)  | Adds support for: <br> *IMX412 image sensors |
| modalai-2-5-2  | Adds support for: <br> * VOXL HDMI Input Accessory (Auvidea B102) <br> * Microchip lan75xx Ethernet devices (VOXL Microhard Add-On v2) <br> |
| modalai-2-3-0  | Updated to new PMD libraries for latest  Time-of-Flight modules (A65) module                                          |
| modalai-2-2-0  | Added kernel driver support for FTDI Serial IO devices                                                                |
| modalai-2-1-0  | Updated to new build architecture (Docker, meta-voxl, meta-voxl-prop) <br>  Adds support for: <br> * WP760x Sierra modems <br> * Microchip lan78xx Ethernet devices <br> *  /bin/ar fix |
| modalai-1-10-0 | Removed CONFIG_ANDROID_PARANOID_NETWORK config from kernel to fix ping permissions problem when running docker images |
| modalai-1-9-0  | Adds support for: <br> * SCTP sockets <br> * IM214 image sensors                                                     |
| modalai-1-7-0  | Increase /data/ parition to 16GB                                                                                      |
| modalai-1-6-0  | Fix for DNS issue, adds virtualization support for Docker                                                             |
| modalai-1-5-0  | Uses open source kernel                                                                                               |
| modalai-1-3-0  | Adds support for: <br> * external cellular modems <br> * PMD Time of Flight cameras over MIPI <br> * UVC (webcams)    |
