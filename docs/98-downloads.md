---
layout: default
title: Downloads
nav_order: 98
has_children: false
permalink: /downloads/
---

## ModalAI Downloads Page

### EULA Downloads

There are files for download that cannot be shared without EULA. As a result you will need to make an account at [developer.modalai.com](https://developer.modalai.com/) to reach the [downloads page](https://developer.modalai.com/asset). Our goal is to make as much as possible open and easily available, but there are some restrictions that we have to follow.

System Images, Software Bundles, and Docker images are available for download here:

[https://developer.modalai.com/asset](https://developer.modalai.com/asset)

Public packages built from public code repositories [http://voxl-packages.modalai.com/stable/](voxl-packages)

#### List

List of proprietary packages available at [https://developer.modalai.com/asset](https://developer.modalai.com/asset)

| Name | File Name | Description |
| --- | --- | --- |
| VOXL System Image | modalai-2-3-0.tar.gz | Version 2.3 (or other) of the VOXL System Image [Details](https://docs.modalai.com/voxl-system-image/) |
| VOXL Factory Bundle | voxl-factory-bundle_1.0.1.tar.gz | Factory Installation Software [Details](https://docs.modalai.com/install-software-bundles/)|
| voxl-emulator docker image | voxl-emulator_v1.1.tgz | VOXL Emulator for software development [Details](https://docs.modalai.com/install-voxl-docker/) |
| 64-bit OpenCL Package for Ubuntu | modalai-opencl-dev_1.0-1.deb | Debian to install on an ARMv8 Ubuntu Docker running on VOXL [Code](https://gitlab.com/voxl-public/voxl-docker-images/voxl-docker-opencv-opencl) |
| snav-modalai | snav-modalai_1.4.1_8x96.ipk | Latest Snapdragon Navigator Release [Details](https://docs.modalai.com/snapdragon-navigator/) |
| roskinetic-xenial docker image | roskinetic-xenial_v1_0.tgz | Ubuntu Docker Image to run on VOXL [Details](https://docs.modalai.com/docker-on-voxl/) |
| Flight Core CAD Files | ModalAI_Flight_Core_V1_CAD.zip | 3D Model for Flight Core |
| VOXL PCB STEP File | VOXL_PCB_connecors_and_some_components.zip | 3D Model for VOXL |
| VOXL Tray 3D STEP File Reference | VOXL-Tray-Reference.STEP | 3D Model for Legacy Qualcomm Flight Pro "Tray" |

### Public Downloads

* OpenCV
  * [OpenCV 3.4.6 32-bit](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_3.4.6_8x96.v7-a.neon.vfpv4.ipk)
  * [OpenCV 3.4.6 64-bit](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_3.4.6_8x96_aarch64.ipk)
  * [OpenCV 4.2.0 64-bit](https://storage.googleapis.com/modalai_public/modal_packages/latest/opencv_4.2.0_8x96_aarch64.ipk)
* [ARM Compute Library 19.08](https://storage.googleapis.com/modalai_public/modal_packages/latest/voxl-armcl_19.08.ipk)
