---
layout: default
title: Collision Prevention
parent: VOXL Vision PX4 for GPS-denied and Indoor Navigation
grand_parent: Flying with VOXL
nav_order: 5
permalink: /voxl-vision-px4-collision-prevention/
---

# VOXL Vision PX4 Collision Prevention
{: .no_toc }



<iframe width="560" height="315" src="https://www.youtube.com/embed/tfI4L5Tj_oc" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


## voxl-vision-px4 Configuration

VOXL needs to be equiped with callibrated stereo cameras to use this feature. If you purchased a Flight Deck or M500 Developer Drone then the cameras should have been calibrated from the factory.

To calibrate yourself, follow the [calibration tutorial](/calibrate-cameras/).


There are three parameters in `/etc/modalai/voxl-vision-px4.conf` to configure the stereo collision prevention. `en_voa` is simply the enable flag. `T_stereo_wrt_body` refers to the position of the left camera relative to the center of mass of the drone. Note the -0.04m offset is Y is the distance the left camera is to the left of the center of mass. The current depth from stereo algorithm outputs a point cloud realiative to the left camera and we use 8cm camera separation on the Flight Deck. 0.10m is the distance the camera is forward of the center of mass.

```json
"en_voa":    true,
"en_send_voa_to_qgc":   false,
"T_stereo_wrt_body":    [0.10, -0.04, 0.0],
```

## PX4 Configuration

The above only enables voxl-vision-px4 to send obastacle data to PX4. PX4 must then be configured to use this data. For full information see the PX4 docs here: [https://docs.px4.io/v1.10/en/computer_vision/collision_prevention.html](https://docs.px4.io/v1.10/en/computer_vision/collision_prevention.html)

We suggest the following parameters for testing this indoors:

```
CP_DIST: 1.5
CP_DELAY: 0.0
CP_GUIDE_ANG: 0.0
CP_GO_NO_DATA: 1
```

The delay can safely be set to 0 since we do accurate timestamping and delay compensation inside voxl-vision-px4. Increasing the delay over 0 will only cause twitchiness and unpredicatable performance.

## Debug Tools

voxl-vision-px4 can be started with the following argument to print obstacle data to the screen before it is sent to PX4.

```bash
-s, --debug_stereo          print detected stereo obstacles as linescan points
```

The parameter `en_send_voa_to_qgc` can be set to `true` in /etc/modalai/voxl-vision-px4.conf to send the same mavlink OBSTACLE_DISTANCE messages to qgroundControl that are are sent to PX4. They can then be plotted with qGroundControl's mavlink_inspector tool.


## Frames of Reference Used

The geometry module inside voxl-vision-px4 keeps track of the following frames of reference.

![px4_chart-geometry_overview](/images/voxl-vision-px4/px4_chart-geometry_overview.png)


voxl-vision-px4 maintains a short history of visual odometry data to compensate for the time delay when computing the depth-from-stereo data as well as the motion and tilt of the drone body.

![px4_chart-stereo_frames](/images/voxl-vision-px4/px4_chart-stereo_frames.png)



## Next

Next page: [Nuttx Shell](/voxl-vision-px4-nuttx-shell/)