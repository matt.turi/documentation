---
layout: default
title: Flying Outdoors without GPS
parent: VOXL Vision PX4 for GPS-denied and Indoor Navigation
grand_parent: Flying with VOXL
nav_order: 7
permalink: /flying-without-gps/
---

# Overview
In this section, we will show you how to fly a mission outdoors without GPS. The drone uses Visual Inertial Odometry (VIO) as outlined in this VOXL Vision [manual](/voxl-vision-px4/). 
The following video demonstration shows an m500 drone that loses GPS during a mission (GPS-denied environment), then solely using vision continues the mission returning to the launch site.

[//]: # "GPS Denied Video"
<iframe width="560" height="315" src="https://www.youtube.com/embed/EstL6hBuxE4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

`Note:` This feature is still in beta. Our team is currently testing PX4 GPS and Vision fusion to ensure stability. Please exercise `caution` when experimenting with this outdoor drone configuration. 


## Set-Up
### 1. PX4 Firmware
In our testing, we're currently using [ModalAI PX4 Firmware v1.10](/flight-core-firmware/#px4-v110).

### 2. VOXL Vision
Ensure you have VOXL Vision ready following the [installation](/voxl-vision-px4-installation/) in this [manual](/voxl-vision-px4/).

### 3. PX4 Parameters
Update the EKF2_AID_MASK. After you update the mask, the values should add up to 329: 

<img src="/images/voxl-vision-px4/qgc-ekf2-param.png" height="200">

### 4. Initialization Procedure
To ensure vision system is stable (during beta testing), take-off with the drone in `manual mode` about 3 meters off of the ground. Hover for approximately 30 seconds then transition into `position mode`. This will merge vision and GPS giving the drone a starting GPS LAT/LON that can be interpolated by the vision system once GPS is no longer available. 

### 5. Disable GPS
Disable GPS using the MAVLink Nuttx console. You can follow the Nuttx console [section](/voxl-vision-px4-nuttx-shell/) in this [manual](/voxl-vision-px4/) or run `gps stop` in QGroundControl: 

<img src="/images/voxl-vision-px4/qgc-console-gps.png" height="200">

Once GPS stops, LAT/LON will no longer update in QGroundControl. The drone will now solely use vision with initialized GPS coordinates.  

### 6. Start Mission
In the [video demo](https://youtu.be/EstL6hBuxE4), we used vision as a failsafe simulating a GPS-denied environment. From here you can start or resume a mission. 




