---
layout: default
title: Flying with VIO for Indoor Navigation
parent: VOXL Vision PX4 for GPS-denied and Indoor Navigation
grand_parent: Flying with VOXL
nav_order: 3
permalink: /flying-with-vio/
---

# Flying with VIO for Indoor Navigation
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---


### Confirm PX4 is Getting VIO Data

As part of the IPK package installation a voxl-vision-px4 systemd service is created which starts the interface and ModalAI Vision Lib automatically on boot. This takes up to about 30 seconds to start. By the time you have plugged in a battery, walked back to your laptop, and opened QGroundControl voxl-vision-px4 should be running. Confirm before flight by opening the mavlink inspector and checking that X&Y values are non-zero in the ODOMETRY mavlink telemetry packets.

![voxl-vio-mavlink-inspector.png](/images/userguides/voxl-vio-mavlink-inspector.png)

Alternatively, if your computer running QGroundControl has its volume turned up you should hear it announce the swap from "manual" to "position" mode when you flip the flight mode switch from up to middle. If VIO data is not being transmitted, the system will fall back to altitude mode and you will hear "position mode rejected, altitude flight mode". In this case, go back to manual flight mode (switch up) and wait a few more seconds for voxl-vision-px4 to finish initializating.

If this doesn't work, go back to Installation Step 1 and reconfirm VIO is working. You can also check the voxl-vision-px4 systemd service for indications of what might have gone wrong.

```bash
adb shell
bash
systemctl status voxl-vision-px4
```



### Confirm Data is in the Right Coordinate Frame

While looking at the ODOMETRY data in QGroundControl's Mavlink Inspector, observe the xyz data as you move the quadcopter around. Make sure it aligns with NED coordinate frame. If there is a coordinate system mismatch PX4 will initially take off then very quickly run away out of control.

TODO: Video/pictures



### Take off in Position Mode

It is safer to take off in Position Flight Mode than it is to flip to this mode mid-flight from Manual Flight Mode.

Take off and land with the same instructions as listed above but this time with the Flight Mode switch in the middle position. The quadcopter should take off straight up and be much easier to control than in manual mode.

Be ready to flip back to manual mode should anything go wrong. It is safer to flip to manual mode and land than to kill the motors mid-flight. Killing the motors mid-flight may result in the propellers loosening from the reverse-torque and flying straight up off the quadcopter. Only kill the motors after landing and spooling down the motors OR in a serious emergency.



### Flip to Offboard Mode

While flying in Position Mode, you may flip the Flight Mode Switch all the way down to enable offboard mode. The quadcopter should immediately fly back to a position 2 meters above wherever VIO initialized. If you plugged in the battery and let the VOXL and PX4 power up at the takeoff location (suggested normal practice) then it should hold a position 2 meters anove the takeoff location.

In Offboard Mode the 2 joysticks on the RC controller are ignored and the PX4 follows any commands given to it by VOXL over the UART link. In this case VOXL just sends a static location.



## Next

Next page: [Apriltag Relocalization](/voxl-vision-px4-apriltag-relocalization/)