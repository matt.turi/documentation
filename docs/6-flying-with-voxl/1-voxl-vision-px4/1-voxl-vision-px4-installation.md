---
layout: default
title: Installation
parent: VOXL Vision PX4 for GPS-denied and Indoor Navigation
grand_parent: Flying with VOXL
nav_order: 1
permalink: /voxl-vision-px4-installation/
---

# VOXL Vision PX4 Installation
{: .no_toc }

Voxl Vision PX4 and its dependencies are installed as part of the [VOXL Software Suite](/install-software-bundles/).

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Configuration Wizard

After installation, VOXL Vision PX4 just needs to be configured with the voxl-configure-vision-px4 script. This will also enable and start the associated systemd service. At the beginning, this script calls voxl-configure-cameras and voxl-configure-modalai-vl to configure its dependencies. It also parses the `/etc/modalai/voxl-vision-px4.conf` config file.

```bash
yocto:/# voxl-configure-vision-px4 -h
General Usage:
voxl-configure-vision-px4

To perform factory configuration for VOXL Flight Deck
voxl-configure-vision-px4 -f

show this help message:
voxl-configure-vision-px4 -h
```

The configuration wizard is ever-evolving as we add new features and new presets for different airframes. You can re-run it at any time after initial setup to set up new features or change configuration.




## Configuration File

Based on your answers to the configuration wizard, an initial parameter set will be entered into the config file. From here you can further configure its settings by editing `/etc/modalai/voxl-vision-px4.conf`.

```json
yocto:/# cat /etc/modalai/voxl-vision-px4.conf
{
    "qgc_ip":   "192.168.8.60",
    "en_localhost_mavlink_udp": true,
    "en_secondary_qgc": false,
    "secondary_qgc_ip": "192.168.1.214",
    "offboard_mode":    "figure_eight",
    "en_vio":   true,
    "en_voa":   false,
    "en_send_vio_to_qgc":   false,
    "en_send_voa_to_qgc":   false,
    "en_adsb":  false,
    "adsb_uart_bus":    7,
    "adsb_uart_baudrate":   57600,
    "R_imu_to_body":    [[0, 1, 0], [1, 0, 0], [0, 0, -1]],
    "T_imu_wrt_body":   [0.068, -0.015, -0.008],
    "height_body_above_ground_m": 0.05,
    "T_stereo_wrt_body":    [0.10, -0.04, 0.0],
    "land_on_tag_id":   0,
    "follow_tag_id":    0,
    "en_apriltag_fixed_frame":  false,
    "fixed_apriltags":  [{
        "tag_id":   0,
        "R_tag_to_fixed":   [[0, -1, 0], [1, 0, 0], [0, 0, 1]],
        "T_tag_wrt_fixed":  [0, 0, 0]
        }],
    "en_transform_mavlink_pos_setpoints_from_fixed_frame":  true,
    "en_fixed_frame_pipe": false,
    "fixed_frame_filter_len": 5
}
```

## Next

For for information on configuring and using voxl-vision-px4, continue reading the remaining sections of the [voxl-vision-px4 manual](/voxl-vision-px4/).

Next page: [Mavlink Telemetry Routing](/voxl-vision-px4-telemetry/)