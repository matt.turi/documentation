---
layout: default
title: Other Features
parent: VOXL Vision PX4 for GPS-denied and Indoor Navigation
grand_parent: Flying with VOXL
nav_order: 99
permalink: /voxl-vision-px4-other-features/
---

# VOXL Vision PX4 Other Features
{: .no_toc }


## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---





## Offboard Figure Eight Mode

Just like the [MAVROS test utility](/mavros/), voxl-vision-px4 can command px4 to fly a figure eight when switched to offboard mode. Don't enable this feature if you are providing offboard mode commands from another source such as MAVROS.

```json
"offboard_mode":    "figure_eight",
```

Note that if apriltag localization is enabled, this will fly the figure eight above the origin of the fixed frame. e.g. it will follow an apriltag around hovering above it. Otherwise it will fly the figure-eight above the origin of local frame, wherever VIO booted up.

To disable voxl-vision-px4's offboard mode for use with your own offboard mode via MAVROS or similar, set the mode to "off".

```json
"offboard_mode":    "off",
```


## ADS-B

Automatic Dependent Surveillance-Broadcast (ADS-B) receive and relay to Ground Controller

```json
"en_adsb":                  false,
"adsb_uart_bus":            7,
"adsb_uart_baudrate":       57600,
```


## Time Synchronization

Synchronizes time between PX4 and VOXL by responding to PX4's timesync mavlink packets. PX4 then filters and maintains an offset between the VOXL's application's processor time (clock_monotonic) and PX4's time. Note that currently these timesync requests are handled directly by voxl-vision-px4 and never make their way to MAVROS as this method provides lower latency and overhead.

