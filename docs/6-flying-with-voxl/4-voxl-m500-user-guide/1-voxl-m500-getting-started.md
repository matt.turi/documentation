---
layout: default
title: VOXL m500 Getting Started
parent: VOXL m500 Developer Drone User Guide
grand_parent: Flying with VOXL
nav_order: 1
has_children: false
permalink: /voxl-m500-getting-started/
---

# VOXL m500 Getting Started
{: .no_toc }

This guide will walk you from taking the VOXL m500 out of the box and up into the air!

For technical details, see the [datasheet](/voxl-m500-reference-drone-datasheet/).

## WARNING
{: .no_toc }

*Unmanned Aerial Systems (drones) are sophisticated, and can be dangerous.  Please use caution when using this or any other drone.  Do not fly in close proximity to people and wear proper eye protection.  Obey local laws and regulations.*

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

### Required Materials

To follow this user guide, you'll need the following:

- Spektrum Transmitter (e.g. SPM8000 or SPMR8000)
- LiPo 4S battery with XT60 connector
  - Example: <https://www.thunderpowerrc.com/products/tp3400-4spx25>
- Host computer with:
  - QGroundControl 3.5.6+
  - Wi-Fi
  - a terminal program

### What's in the Box

Included in the box are:

- VOXL m500 with Spektrum Reciever (note: the system supports FrSky receivers like X8R, see [below](#rc-radio-setup))
- 10" props
- USB-to-JST Cable (not used in this guide)
- Spare landing gear mounting screws

![m500-out-of-box](/images/userguides/m500/m500-out-of-box.jpg)

### Vehicle Components

The following images give an overview of the vehicle components.

![vehicle-overview-front-144-1024.jpg](/images/userguides/m500/vehicle-overview-front-144-1024.jpg)

![vehicle-overview-back-zoom-144-1024.jpg](/images/userguides/m500/vehicle-overview-back-zoom-144-1024.jpg)

![vehicle-overview-right-144-1024.jpg](/images/userguides/m500/vehicle-overview-right-144-1024.jpg)

![vehicle-overview-left-144-1024.jpg](/images/userguides/m500/vehicle-overview-left-144-1024.jpg)






## Connecting qGroundControl over WiFi

For setup and configuration of your M500 you must **Remove the Propellers** for safety.

### Power on VOXL and Flight Core

Now power up the VOXL and Flight Core on the M500. This can be done by connecting a [5V Power Supply](https://www.modalai.com/products/ps-xt60) to the power module's barrel jack. Or you can plug in a 4S battery:

To install a 4S battery, slide into the body as shown.  There is a stop at the front of the vehicle that will prevent the battery from sliding too far forward. The battery should be pressed all the way in until it hits the stop for a consistent center of mass. Power on the vehicle by connecting the battery to the M500's XT60 connector.

![vehicle-battery-2-144-1024.jpg](/images/userguides/m500/vehicle-battery-2-144-1024.jpg)


### Connect to VOXL Access Point

From the factory, the vehicle will power up as a WiFi Access Point with a name like `VOXL:XX:XX:XX:XX:XX`).  You can connect to it with a password of `1234567890`.

![voxl-ap-list.png](/images/userguides/m500/voxl-ap-list.png)

![voxl-ap-password.png](/images/userguides/m500/voxl-ap-password.png)

Once connected to the VOXL AP, you'll get an IP address from VOXL via DHCP, locate the address, in my case it's `192.168.8.51`.

![voxl-ap-ip-address.png](/images/userguides/m500/voxl-ap-ip-address.png)

More details can be found at the [VOXL WiFi quickstart page](/wifi-setup/).


### SSH into VOXL

The VOXL's IP address by default is `192.168.8.1`.  SSH into VOXL using the `root` user with password `oelinux123`.

More details available at the [VOXL SSH quickstart page](/ssh-to-voxl/).

![voxl-ssh.png](/images/userguides/m500/voxl-ssh.png)


### Edit voxl-vision-px4.conf

VOXL needs to know the IP address of your computer running qGroundControl. This is set in the 'qgc_ip' field of the `/etc/modalai/voxl-vision-px4.conf` file on VOXL.  This will inform the `voxl-vision-px4` systemd service to send MAVLink data to our computer.

Run the following command and update the `qgc_ip` field to match your computer's IP.

```bash
yocto:/home/root# vi /etc/modalai/voxl-vision-px4.conf
```

![voxl-vi-cmd-line.png](/images/userguides/m500/voxl-vi-cmd-line.png)

Hit `i` to enter edit mode, use arrow keys to navigate to the `qgc_ip` field and edit there. To save, hit `esc` then `:wq` to write and quit.

![voxl-vi-edit.png](/images/userguides/m500/voxl-vi-edit.png)

If you put VOXL on your own WiFi or LTE network you will have to do the same procedure.

For more information on this config file and the options available see the [VOXL Vision PX4 manual](/voxl-vision-px4-features/).


### Restart the voxl-vision-px4 Service

To reset the `voxl-vision-px4` service and reload the configuration, run the following command:

```bash
yocto:/home/root# systemctl restart voxl-vision-px4
```

Note this will also reset the Visual Inertial Odometry (VIO) algorithm and reset VIO's origin to wherever the drone is when you reset the service.


### Confirm Connection with QGroundControl

After the previous step is completed, the vehicle should automatically connect to QGroundControl

![voxl-qgc-connected.png](/images/userguides/m500/voxl-qgc-connected.png)


## Sensor Calibration
VOXL-M500 flight sensors are delivered pre-calibrated from the factory. PX4 will warn you if your compass needs to be re-calibrated depending on your location and environmental conditions. If so, or if you'd like re-calibrate all sensors, follow the [px4 sensor calibration precedure](/px4-calibrate-sensors/).

## RC Radio Setup

Now you need to bind and calibrate your DSM RC radio through the qGroundControl interface.

**NOTE:** the following assumes using a Spektrum reciever and transmitter.  It's possible to use a FrSky receiver on the m500, such as the X8R, using [J1004](https://docs.modalai.com/voxl-flight-datasheet-connectors/#j1004---rc-input--spektrumsbususart6-connector) and 5V power from a port like [J1003](https://docs.modalai.com/voxl-flight-datasheet-connectors/#j1003---ppm-rc-in) and a cable like [MCBL-00018](https://docs.modalai.com/cable-datasheets/#mcbl-00018)

See [this](/flight-core-connections/#rc-receiver) for more details.

### Bind to Spektrum

Instructions on how to bind a Spektrum radio can be found [here](https://docs.px4.io/v1.9.0/en/config/radio.html).

### Calibrate Radio

Now follow the on-screen instructions to calibrate the range and trims of your radio.

![8-calibrate-radio.png](/images/qgc/8-calibrate-radio.png)

### Confirm RC Settings

Obviously every user will want to use different flight modes and different switch assignments, but for getting started with VOXL and and Flight Core we suggest starting with something similar to this configuration and working from there.

- "Flap Gyro" switch left of Spektrum Logo
  - Channel 6
    - Up position:     Manual Flight Mode
    - Middle Position: Position Flight Mode
    - Down Position:   Offboard Flight mode

- "Aux2 Gov" switch right of Spektrum Logo
  - Channel 7
    - Up position:     Motor Kill Switch Engaged
    - Down Position:   Motor Kill Switch Disengaged (required to fly)

Since we have a manual kill switch on the radio there is no need for the "safety switch" on the Pixhawk GPS module as so it is disabled in our params file in favor of the kill switch.

![9-flight-mode-config.png](/images/qgc/9-flight-mode-config.png)

If you have a Spektrum DX8 radio with a clean acro-mode model you can accomplish the above channel mapping by loading the following config file

[https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params](https://gitlab.com/voxl-public/px4-parameters/-/blob/master/params/spektrum_dx8_config.params)

Still confirm the mapping in qGroundControl before flight!



## Preflight Checks

Now disconnect the 5V power supply or battery you used during the configuration steps. Move the M500 to a safe location where you wish to fly and connect a battery for flight while the M500 is on the ground.

The position and direction VOXL powers up in will dictate the origin and orientation of the Visual Inertial Odometry (VIO) coordinate frame, also known as LOCAL_POSITION_NED in PX4 terminology. VOXL will correct for any tilt due to non-level ground.


### Attitude Check

Lift the vehicle and move it around, verify that the attitude reported in QGroundControl GUI looks and responds correctly. Try not to cover the tracking camera during this process.

![qgc_attitude_gui.png](/images/qgc/qgc_attitude_gui.png)


### VIO Data Validation

In QGroundControl, go to `Widgets > MAVLink Inspector`.  Find the `LOCAL_POSITION_NED` node in the list, and validate the data as follows.

The local coordinate frame in PX4 is right-handed, with X pointing forward out the drone, Y pointing to the right, and Z-axis down in units of meters. This is also known as FRD frame (forward, right, down).

It should show roughly “0 0 0” if you placed the M500 back down where it powered up.  The origin and orientation is initialized when the voxl-vision-px4 service starts. You can re-initialize by restarting the service as described before if you want to move to a new flight location.

![voxl-vio-mavlink-inspector.png](/images/userguides/m500/voxl-vio-mavlink-inspector.png)

- Lift the vehicle straight up a meter, validating that the Z measurement gets more negative, roughly 1 meter
- Walk forward 1 meter, validating that the X measurement gets more positive, roughly 1 meter
- Walk to your right 1 meter, validating that the Y measurement gets more positive, roughly 1 meter
- Place the vehicle back in the takeoff position, and validate that you are back to roughly “0 0 0


### Arming Vehicle Without Props

With the propellers **off** arm the vehicle.

- Set killswitch to off ("Aux2 Gov" switch down)
- **Left** stick hold **down** and to the **right**

Validate the motor rotation is as expected and the vehicle responds to throttling up momentarily.

Disarm the vehicle:

- **Left** stick hold **down** and to the **left**

Correct rotation direction of motors:

![QuadRotorX.svg](/images/qgc/QuadRotorX.svg)



### Install Propellers

Install the propellers following this orientation. Each motor shaft has a white or black top to indicate which propeller goes where. Note that the clockwise spinning propellers are reverse threaded to prevent loosening during flight.

Ensure you tighten the propellers down tightly by hand or they may spin off when engaging the kill switch.

![vehicle-overview-props-144-1024.jpg](/images/userguides/m500/vehicle-overview-props-144-1024.jpg)



## First Flights

### First Flight (Manual Mode)

You should be comfortable flying before proceeding!

Arm the vehicle, now safely fly in manual mode! No instructions here, you should know what you are doing if you're flying!

Land and disarm.



### Second Flight (Position Mode)

The following will have you take off and fly in position flight mode. You should be comfortable flying before proceeding!

- Check the VIO data by again looking at `Widgets > MAVLink Inspector` and ensure the `LOCAL_POSITION_NED` X Y and Z values look consistent with where the vehicle has landed.

- On your RC Radio flip to position mode. If using the DX8 config described before, that will be accomplished by moving ghe flap_gyro switch to the middle position.

- Validate that QGroundControl recognizes you are in `Position` mode

- Arm the vehicle

- To take off, move the throttle to 50% or the "hover" position then slowly increase throttle above %50 until the M500 takes off. Note that full throttle will raise the vehicle rapidly at 3 m/s (no throttle will drop at 1 m/s)

- Having the sticks at 'center' position will hold the vehicle in place. Once the feedback controller settles, you should be able to leave the throttle stick in the middle position, and the M500 will hold still with no user input.

- Land and disarm the vehicle

