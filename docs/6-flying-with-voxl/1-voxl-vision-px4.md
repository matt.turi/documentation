---
layout: default
title: VOXL Vision PX4 for GPS-denied and Indoor Navigation
parent: Flying with VOXL
nav_order: 1
has_children: true
permalink: /voxl-vision-px4/
---

# VOXL Vision PX4 for GPS-denied and Indoor Navigation

The voxl-vision-px4 package provides the software interface between the VOXL and PX4 flight controller. It enables GPS-denied position control via Visual Inertial Odometry (VIO), Visual Obstacle Avoidance (VOA), fiducial marker relocalization using AprilTags, and many other features.

[Source Code](https://gitlab.com/voxl-public/voxl-vision-px4/)

<iframe width="560" height="315" src="https://www.youtube.com/embed/wrqZFTiHyTU" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

![px4_chart-architecture](/images/userguides/px4_chart-architecture.png)


