---
layout: default
title: Flight Test Mission Brief
parent: Flying with VOXL
nav_order: 100
has_children: false
permalink: /mission-brief/
---

# Flight Test Mission Brief
This guide is meant to help you during the planning stage of your flight test. This is not an exhaustive list but rather an example of what the flight testing crew at ModalAI use.

You can download the complete brief [here](/images/resources/flight-testing-mission-brief.docx).

# Mission Brief

- Objective
- Mission Plan
- Hazards/Contingency/Weather
- Roles
- Emergency Actions
- Communications
- Safety
- Discrepancies
- Any Questions

Here is a brief description of each item:

__Objective__

- Identify the purpose/goal of the flight so the entire crew understands 

__Mission Plan__

- Identify GCS/home point, launch point, describe the flight path and what the aircraft will do from start to finish.
- Identify what the Remote Pilot in Command (RPIC) will be doing or looking for during the flight.

__Hazards/Contingency/Weather__

- Identify any hazards such as power lines, people, roads, etc.
- Identify any contingencies during flight such as alternate landing spots, what to do if a manned aircraft comes into your airspace, if people come into your flight path, etc.
- Describe any weather that might affect your mission, aircraft or crew such as extreme hot or cold weather for humans that could cause sunburn or dehydration.

__Roles__

- Identify the role each crew member will play.

__Emergency Actions__

- Identify actions taken by RPIC and crew for in-flight emergencies (Bad GPS, loss of motor, etc)
- Identify actions by RPIC and crew if there is a fire or medical emergency (Who will have the keys to the vehicle, who will have grid location for fire department, who will have the fire extinguisher).

__Communications__

- Identify primary, alternate, and emergency means of communication
- Identify how you will call 911

__Safety__

- Identify where the nearest hospital is, who has the keys to the vehicle, where the first aid kit is.

__Discrepancies__

- Anything out of the ordinary with the aircraft, crew, or mission? 

__Any Questions__

- Top to bottom, everyone should be crystal clear on the plan.
