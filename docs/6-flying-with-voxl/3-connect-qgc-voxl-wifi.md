---
layout: default
title: Connect QGroundControl to VOXL using WiFi
parent: Flying with VOXL
nav_order: 3
has_children: false
permalink: /qgc-wifi/
youtubeId: bQraPU4pFiY
---

# How to Connect QGroundControl to VOXL using WiFi
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

The VOXL system can connect to QGroundControl through an IP connection, and an easy way to do this is through the system's built in WiFi capability.

Specifically, the MAVLink protocol is used to communicate between the Flight Core portion of the VOXL architecture running PX4 and QGroundControl running on a host computer or Ground Control Station.

Using the VOXL Flight solution, this is acheived with a single board hardware solution.  Using the VOXL + Flight Core solution, this is done using a 'split board' solution connected via a serial cable.  

In both solutions, we utilize the [voxl-vision-px4](https://gitlab.com/voxl-public/voxl-vision-px4) service to facility the MAVLink over IP connection to QGroundControl (and more behind the scenes, the MAVLink over UART to PX4).  More details about `voxl-vision-px4` can [be found here](/voxl-vision-px4/).

![voxl-vision-px4.png](/images/voxl-vision-px4/voxl-vision-px4-graphic.png)

### Youtube Tutorial

For those of you who prefer videos, we have a video tutorial here that is the long-winded version of the text below!

TL;DR

{% include youtubePlayer.html id=page.youtubeId %}

## Detailed Steps

Below describes how to setup the connection over Wi-Fi.  It should be noted, this can be acheived as well over a Microhard or LTE wireless link.

### Requirements

- VOXL Flight or VOXL + Flight Core
- WiFi antennas
- Host system with QGroundControl 3.5.6+

If using the VOXL + Flight Core solution, you can use [the hardware guide connection](/how-to-connect-voxl-to-flight-core/) on how to connect the two together.

### Select Wi-Fi Connection Type

For both methods, ensure that the Wi-Fi antennas are attached.  These are included in both the [VOXL-DK]() and [VOXLF-DK]() kits as PN `MANT-00001` and found on [Digikey](https://www.digikey.com/product-detail/en/molex/1461531100/WM17824-ND/8543422).

More details can be found at the [VOXL WiFi quickstart page](/wifi-setup/).

#### Access Point Mode

From the factory, the vehicle will power up as a WiFi Access Point with a name like `VOXL:XX:XX:XX:XX:XX`).  You can connect to it with a password of `1234567890`.

If wanting to use this mode but the device is not showing up, you can connect to the device over `adb` and use the `voxl-wifi softap` command to configure the device into AP mode.

![voxl-ap-list.png](/images/userguides/m500/voxl-ap-list.png)

![voxl-ap-password.png](/images/userguides/m500/voxl-ap-password.png)

#### Station Mode

Alternatively, you can configure the device into `Station` mode by connecting to the device over `adb` and using the `voxl-wifi station` command to connect to a network of your choosing.

### Ground Control Station IP

Once connected to the VOXL AP (or alternatively if using `Station` mode and on a network), you'll get an IP address from VOXL via DHCP.  You need to locate the address, for this user guide, it's `192.168.8.51`.

![voxl-ap-ip-address.png](/images/userguides/m500/voxl-ap-ip-address.png)

### voxl-vision-px4

We'll use the `voxl-vision-px4` service to faclitate the connection between PX4 and QGroundControl.  If the service has been configured already, there will be a configuration file at `/etc/modalai/voxl-vision-px4.conf`.  You'll want to edit this file and set the `qgc_ip` field to the IP address located in the previous step.

After modifying the file, you can restart the service using `systemctl restart voxl-vision-px4` and the connection should be established.

If the file doesn't exist, we can setup the service using the `voxl-configure-vision-px4` command.  During this setup, you'll be asked to enter the IP address for QGroundControl.  After the setup wizard is finished, the connection should be established.

Details about `voxl-vision-px4` installation can [be found here](/voxl-vision-px4/).

### Flight Core MAVLink and Serial Settings

No configuration should be required, but if settings have been modifed, we've captured them here.  By default, both the Flight Core and VOXL-Flight will be configured with the following:

- `MAV_1_CONFIG` set to `TELEM 2`
- `MAV_1_FORWARD` set to `0`
- `MAV_1_MODE` set to `Normal`
- `MAV_1_RATE` set to `0 B/s` (max for the baud)

and:

- `SER_TEL2_BAUD` set to `921600 8N1`

This will enable the proper serial/MAVLink connection for the VOXL Flight and for Flight Core `J1` to VOXL `J12`.  It should be noted that this is not configurable for the VOXL Flight as the connection is internally routed.
