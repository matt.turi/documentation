---
layout: default
title: VOXL VPN Manual
parent: VOXL Add-Ons
nav_order: 3
permalink: /voxl-vpn/
---

# VOXL VPN Manual
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

---

## Overview

A typical use case of an LTE connection is to connect a drone to a Ground Control Station (GCS) such as [QGroundControl](http://qgroundcontrol.com/). Typically the GCS is also connected to an LTE network. In this case, both the drone and the GCS have been assigned IP addresses on the LTE network that are private to the carrier network. This prevents the drone from directly connecting to the GCS since their IP addresses are not visible to each other over the internet. 

There are a variety of ways to solve this issue. ModalAI uses a VPN as the preferred solution.

![LTE data flow](/images/userguides/lte-to-lte-data-flow.png)

This diagram shows how the VPN solution works. In this example a server is allocated with a static IP address, `35.236.55.229`. You can easily set your own server up with [Google Cloud Platform](https://cloud.google.com/), [Amazon Web Services](https://aws.amazon.com/), [Microsoft Azure](https://azure.microsoft.com/en-us/), etc. Ubuntu is our preferred OS for our servers. Our VPN is setup using the [OpenVPN](https://openvpn.net/) software package.

In the example above, once the drone has connected to the AT&T network, it obtains the IP address `10.47.x.y` and the GCS, once connected, obtains the address `10.47.x.z`.

With the OpenVPN server software running on the cloud server and OpenVPN client software on both the drone and the GCS, the devices can now connect and get VPN IP address assignments. In this diagram, the drone is assigned `10.8.0.6` and the GCS is assigned `10.8.0.8`. The drone can now communicate directly to the GCS using the `10.8.0.8` IP address.

It is desirable for the drone and the GCS to always get the same IP address when connecting to the VPN. This is possible by assigning each separate network endpoint a unique security certificate. When each endpoint connects using it's certificate it can be configured to receive the same address every time. 

## OpenVPN client on VOXL

The base VOXL System Image comes with OpenVPN pre-installed

In order to use OpenVPN on VOXL, you will need to supply a configuration file and the certificates / keys for your VPN. These files can be placed in the directory `/etc/openvpn`. 

The following is an example of running OpenVPN as a daemon:

```bash
cd /etc/openvpn

openvpn --daemon --script-security 2 --config vpn-key.ovpn
```

**NOTE**: The connection to the VPN will fail unless the date and time have been correctly set. This is required due to the dates emedded in the certificates. [voxl-time-sync](https://gitlab.com/voxl-public/voxl-time-sync) is a script that will run until the date and time have been set correctly and can be enabled to start on boot.

The default version of OpenVPN on VOXL is `2.1.3`. Newer versions are abailable and compile nicely on target. Here is an example of setting up version `2.4.6`:

```bash
git clone https://github.com/OpenVPN/openvpn

cd openvpn
git checkout tags/v2.4.6

autoreconf -i -v -f

./configure --disable-lzo --disable-plugin-auth-pam --prefix=/usr/local

make -j4

make install
```
