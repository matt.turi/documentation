---
layout: default
title: USB Expander and Debug Add-On Board Manual
parent: VOXL Add-Ons
nav_order: 6
permalink: /usb-expander-and-debug-manual/
---

# USB Expander and Debug Add-On Board Manual
{: .no_toc }

## Table of contents
{: .no_toc .text-delta }

1. TOC
{:toc}

## Summary

The VOXL USB Expander and Debug Add-On board provides:

- Ability to connect an additional USB2 peripheral device to VOXL
- A debug console for low level system debugging
- A switch that will force VOXL to boot into `fastboot` mode (for reflashing the [System Image](/flash-system-image/) if the unit is bricked in the field)
- A switch that will force VOXL to boot into `emergency boot` mode (used at the factory only)

## Board Connections

![voxl-usb-expander-and-debug.png](../../images/datasheet/voxl-usb-expander-and-debug.png)

## Connecting to VOXL

To connect the USB Expander and Debug Add-On board to VOXL:

- Attach the daughter board's `J1` connector to VOXL's `J13` connector

![voxl-usb-expander-and-debug-connected.png](../../images/datasheet/voxl-usb-expander-and-debug-connected.png)

## Connecting a USB2 Peripheral

A USB2 peripheral can be connected via a `JST GHR-04V-S` connector following the pinouts specified in the [datasheet](usb-expander-and-debug-datasheet/).

To validate the connection:

- Power off the VOXL
- Attach the USB Expander and Debug Add-On board
- Ensure the `FASTBOOT` labeled switch is set to **off**
- Ensure the `EMERG BOOT` labeled switch is set to **off**
- Attach the USB2 peripheral
- Power on the VOXL
- Connect a Micro USB cable to VOXL's USB port and connect to a Linux PC

Open the terminal in the Linux PC and run the following:

```bash
adb shell
```

Now you're inside VOXL, run the following, where in this use case we've attached a Flight Core board to the USB2 Host connector (showing up as the `STMicroelectronics` MCU):

```bash
/ # lsusb
...
Bus 001 Device 003: ID 0483:a32f STMicroelectronics
...
```

## Forcing the VOXL into Fastboot to Unbrick
Follow the unbrick procedure found at [docs.modalai.com/unbrick-voxl](https://docs.modalai.com/unbrick-voxl)

## Using the Debug Console

You'll need two MicroUSB cables for this procedure.

- Power off the VOXL
- Attach the USB Expander and Debug Add-On board
- Ensure the `FASTBOOT` labeled switch is set to **off**
- Ensure the `EMERG BOOT` labeled switch is set to **off**
- Connect a Micro USB cable to VOXL's USB port and connect to a Linux PC
- Connect a second Micro USB cable to the Add-On board's Micro USB port and connect to a Linux PC.  It may display as `FT232R USB UART` and will exist as `/dev/ttyUSB0`
- Power on the VOXL

Open `Terminal A` in the Linux PC and run the following command (if `screen` isn't installed, first run `apt install screen`):

```bash
screen /dev/ttyUSB0 115200
[HIT ENTER]
```

Open `Terminal B` in the Linux PC and run the following commands:

```bash
adb shell
reboot
```

You should now see debug information in `Terminal A`:

```bash
[10] Opening SSR Glink Port success
[20] Glink Connection between APPS and RPM established
[20] Glink Connection between APPS and RPM established
[40] UFS init success
[80] pm8x41_get_is_cold_boot: cold boot
[90] Unable to locate /bootselect partition
[90] Loading (boot) image (57376768): start
[300] Loading (boot) image (57376768): done
[310] use_signed_kernel=1, is_unlocked=1, is_tampered=0.
[310] decompressing kernel image: start
[630] decompressing kernel image: done
[640] Best match DTB tags 291/00000008/0x00000003/30001/20009/10013/0/0/940319e2/51297
[650] Using pmic info 0x20009/0x10013/0x0/0x0 for device 0x20009/0x10113/0x0/0x0
[650] cmdline: noinitrd rw console=ttyHSL0,115200,n8 androidboot.hardware=qcom ehci-hcd.park=3 msm_rtb.filter=0x37 lpm_levels.sleep_disabled=0 rootwait net.ifnames=0 androidboot.bootdevice=624000.ufshc androidboot.serialno=a0259b4a androidboot.baseband=apq root[670] Updating device tree: start
[680] Updating device tree: done
[680] RPM GLINK UnInit
[690] booting linux @ 0x80080000, ramdisk @ 0x82200000 (0), tags/device tree @ 0x82000000
[690] Jumping to kernel via monitor
```


## Forcing the VOXL into Emergency Boot

This is typically done at the factory and not in the field by a normal user.  To force the VOXL to boot into `emergency boot` mode:

- Power off the VOXL
- Attach the USB Expander and Debug Add-On board
- Ensure the `FASTBOOT` labeled switch is set to **off**
- Ensure the `EMERG BOOT` labeled switch is set to **on**
- Attach
- Power on the VOXL

The device will show up on a Linux PC as `Qualcomm, Inc. Gobi Wireless Modem`:

```bash
/ lsusb
...
Bus 001 Device 015: ID 05c6:9008 Qualcomm, Inc. Gobi Wireless Modem (QDL mode)
...
```
