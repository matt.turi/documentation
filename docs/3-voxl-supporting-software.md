---
layout: default
title: VOXL Supporting Software
nav_order: 3
has_children: true
permalink: /voxl-supporting-software/
---

# VOXL Supporting Software
{: .no_toc }

## Overview

VOXL's software stack is broken up into 3 parts, each with their own version and ability to upgrade independently or all together.

- [VOXL System Image](/voxl-system-image/): This includes the root file system and bootloader partitions. It is only necessary to update the system image to add new hardware driver support.
- VOXL Factory Bundle: This contains proprietary and 3rd party software packages (updated infrequently).
- [voxl-suite](https://gitlab.com/voxl-public/voxl-suite): precompiled packages of ModalAI-developed open-source software (updated regularly)

The System Image and Factory Bundle are available at [https://developer.modalai.com/asset](https://developer.modalai.com/asset).  The `voxl-suite` package uses an OPKG package manager and is hosted [here](http://voxl-packages.modalai.com/stable/).  The installer at the link below will offer to install the `voxl-suite` for you as the last step.

All three packages are loaded on VOXL boards at the factory but can be updated by the user at any time using the instructions in [VOXL Quickstarts](/voxl-quickstarts/).

## voxl-suite

The `voxl-suite` packages are [described and maintaned here](https://gitlab.com/voxl-public/voxl-suite).

## VOXL Factory Bundle

### v1.0.1

*Note: System Image 2.3.0+ is required to use Factory Bundle v1.0.1*

Package list:

| Name                        | Package                                          | Notes                                                                             |
|:----------------------------|:-------------------------------------------------|:----------------------------------------------------------------------------------|
| aufs-util                   | aufs-util_3.14+git0+b59a2167a1-r0_aarch64.ipk    | Required for Docker on VOXL                                                       |
| avahi-daemon                | avahi-daemon_0.6.31-r11.1_aarch64_js.ipk         |                                                                                   |
| cgroup-lite                 | cgroup-lite_1.1-r0_aarch64.ipk                   | Required for Docker on VOXL                                                       |
| royale-331-spectre-4-7      | latest_royale-331-spectre-4-7_0.0.1_8x96.ipk     | Required for A65 Time-of-Flight                                                   |
| lib32-git-perltools         | lib32-git-perltools_2.5.0-r0_armv7a-vfp-neon.ipk |                                                                                   |
| libavahi-common3            | libavahi-common3_0.6.31-r11.1_aarch64.ipk        |                                                                                   |
| libavahi-core7              | libavahi-core7_0.6.31-r11.1_aarch64.ipk          |                                                                                   |
| libcgroup                   | libcgroup_0.41-r0_aarch64.ipk                    | Required for Docker on VOXL                                                       |
| libdaemon0                  | libdaemon0_0.14-r0_aarch64.ipk                   |                                                                                   |
| libncurses                  | libncursesw5_5.9-r15.1_aarch64.ipk               | Required for Docker on VOXL                                                       |
| libnss-mdns                 | libnss-mdns_0.10-r7_aarch64.ipk                  |                                                                                   |
| libsmartcols                | libsmartcols1_2.26.2-r0_aarch64.ipk              | Required for Docker on VOXL                                                       |
| libsystemd                  | libsystemd0_225+git0+e1439a1472-r0_aarch64       | Required for Docker on VOXL                                                       |
| Qualcomm Machine Vision SDK | mv_1.2.8_8x96.ipk                                | [Qualcomm Developer Network](https://developer.qualcomm.com/software/machine-vision-sdk machine-vision-sdk) |
| ROS Indigo Build Deps       | ros-indigo-build-deps_1.0.1.ipk                  | [Source](https://gitlab.com/voxl-public/ros-indigo-build-deps)                    |
| util-linux                  | util-linux_2.26.2-r0_aarch64_js.ipk              | Required for Docker on VOXL                                                       |
| util-linux-losetup          | util-linux-losetup_2.26.2-r0_aarch64.ipk         | Required for Docker on VOXL                                                       |
| util-linux-swaponoff        | util-linux-swaponoff_2.26.2-r0_aarch64.ipk       | Required for Docker on VOXL                                                       |
| util-linux-umount           | util-linux-umount_2.26.2-r0_aarch64.ipk          | Required for Docker on VOXL                                                       |
| voxl-utils                  | voxl-utils_0.5.1.ipk                             | [Source](https://gitlab.com/voxl-public/voxl-utils)                               |

### v0.0.3, v0.0.4, v0.0.5

*Note: the factory version is bumped to stay in sync with software bundle version*

Package list:

| Name                        | Package                                          | Notes                                                                             |
|:----------------------------|:-------------------------------------------------| :----------------------------------------------------------------------------------|
| aufs-util                   | aufs-util_3.14+git0+b59a2167a1-r0_aarch64.ipk    | Required for Docker on VOXL                                                       |
| avahi-daemon                | avahi-daemon_0.6.31-r11.1_aarch64_js.ipk         |                                                                                   |
| cgroup-lite                 | cgroup-lite_1.1-r0_aarch64.ipk                   | Required for Docker on VOXL                                                       |
| ffmpeg                      | ffmpeg_3.4.5_8x96_js.ipk                         | [Source](https://gitlab.com/voxl-public/voxl-ffmpeg)                              |
| lib32-git-perltools         | lib32-git-perltools_2.5.0-r0_armv7a-vfp-neon.ipk |                                                                                   |
| libavahi-common3            | libavahi-common3_0.6.31-r11.1_aarch64.ipk        |                                                                                   |
| libavahi-core7              | libavahi-core7_0.6.31-r11.1_aarch64.ipk          |                                                                                   |
| libcgroup                   | libcgroup_0.41-r0_aarch64.ipk                    | Required for Docker on VOXL                                                       |
| libdaemon0                  | libdaemon0_0.14-r0_aarch64.ipk                   |                                                                                   |
| libncurses                  | libncursesw5_5.9-r15.1_aarch64.ipk               | Required for Docker on VOXL                                                       |
| libnss-mdns                 | libnss-mdns_0.10-r7_aarch64.ipk                  |                                                                                   |
| libsmartcols                | libsmartcols1_2.26.2-r0_aarch64.ipk              | Required for Docker on VOXL                                                       |
| libsystemd                  | libsystemd0_225+git0+e1439a1472-r0_aarch64       | Required for Docker on VOXL                                                       |
| Qualcomm Machine Vision SDK | mv_1.2.8_8x96.ipk                                | [Qualcomm Developer Network](https://developer.qualcomm.com/software/machine-vision-sdk machine-vision-sdk) |
| ROS Indigo Build Deps       | ros-indigo-build-deps_1.0.1.ipk                  | [Source](https://gitlab.com/voxl-public/ros-indigo-build-deps)                    |
| util-linux                  | util-linux_2.26.2-r0_aarch64_js.ipk              | Required for Docker on VOXL                                                       |
| util-linux-losetup          | util-linux-losetup_2.26.2-r0_aarch64.ipk         | Required for Docker on VOXL                                                       |
| util-linux-swaponoff        | util-linux-swaponoff_2.26.2-r0_aarch64.ipk       | Required for Docker on VOXL                                                       |
| util-linux-umount           | util-linux-umount_2.26.2-r0_aarch64.ipk          | Required for Docker on VOXL                                                       |

You can check the version of the VOXL Factory Bundle by looking at the version file.

```bash
cat /etc/modalai/voxl-factory-bundle-version.txt
0.0.5
```

## VOXL Software Bundle

*Note: as of System Image 2.3.0, this is considered legacy.  It is replaced with the [voxl-suite](https://gitlab.com/voxl-public/voxl-suite) packages*

### V0.0.5

Changes:

- Added support for ICM-42688 in imu_app_0.0.5_8x96.ipk for factory test

The package list:

| Name                                       | Package                        | Notes                                                        |
|:-------------------------------------------|:-------------------------------|:-------------------------------------------------------------|
|                                            | cellular_tools_0.2.0_8x96.ipk  |                                                              |
| [Docker](/docker-on-voxl/)                 | docker_1.9.0+git...aarch64.ipk |                                                              |
| [IMU app](https://docs.modalai.com/snap-imu/)                                    | imu_app_0.0.5_8x96.ipk         | Soon to be replaced by voxl-imu                              |
| [RC Math Library](/librc-math/)            | librc_math_1.0.1.ipk           | [Source](https://gitlab.com/voxl-public/librc_math)          |
| [VOXL IO Library](/libvoxl-io/)            | libvoxl_io_0.0.6.ipk           | [Source](https://gitlab.com/voxl-public/libvoxl_io)          |
| [ModalAI Vision Lib](/modalai-vision-lib/) | modalai-vl_0.1.0_8x96.ipk      |                                                              |
| [VOXL Camera Manager](/voxl-cam-manager/)  | voxl-cam-manager_0.2.1.ipk     | [Source](https://gitlab.com/voxl-public/voxl-cam-manager)    |
| [VOXL Docker Support](/docker-on-voxl/)    | voxl-docker-support_1.1.0.ipk  | [Source](https://gitlab.com/voxl-public/voxl-docker-support) |
| [VOXL IMU](/voxl-imu/)                     | **voxl_imu_0.0.4_8x96.ipk**    |                                                              |
| [VOXL ROS Nodes](/ros-nodes-and-usage/)    | voxl-nodes_0.0.6_8x96.ipk      | [Source](https://gitlab.com/voxl-public/voxl-nodes)          |
| [VOXL RTSP](https://docs.modalai.com/voxl-rtsp/)                                | voxl-rtsp_1.0.0_8x96.ipk       | [Source](https://gitlab.com/voxl-public/voxl-rtsp)           |
| [VOXL Utils](/voxl-utils/)                 | voxl-utils_0.4.6.ipk           | [Source](https://gitlab.com/voxl-public/voxl-utils)          |
| [VOXL Vision PX4](/voxl-vision-px4/)       | voxl-vision-px4_0.4.1_8x96.ipk | [Source](https://gitlab.com/voxl-public/voxl-vision-px4)     |

### V0.0.4

Changes:

- Improvements in voxl-vision-px4

The package list:

| Name                                       | Package                            | Notes                                                        |
|:-------------------------------------------|:-----------------------------------|:-------------------------------------------------------------|
|                                            | **cellular_tools_0.2.0_8x96.ipk**  |                                                              |
| [Docker](/docker-on-voxl/)                 | docker_1.9.0+git...aarch64.ipk     |                                                              |
| [IMU app](https://docs.modalai.com/snap-imu/)                                    | **imu_app_0.0.5_8x96.ipk**         | Soon to be replaced by voxl-imu                              |
| [RC Math Library](/librc-math/)            | librc_math_1.0.1.ipk               | [Source](https://gitlab.com/voxl-public/librc_math)          |
| [VOXL IO Library](/libvoxl-io/)            | libvoxl_io_0.0.6.ipk               | [Source](https://gitlab.com/voxl-public/libvoxl_io)          |
| [ModalAI Vision Lib](/modalai-vision-lib/) | **modalai-vl_0.1.0_8x96.ipk**      |                                                              |
| [VOXL Camera Manager](/voxl-cam-manager/)  | voxl-cam-manager_0.2.1.ipk         | [Source](https://gitlab.com/voxl-public/voxl-cam-manager)    |
| [VOXL Docker Support](/docker-on-voxl/)    | voxl-docker-support_1.1.0.ipk      | [Source](https://gitlab.com/voxl-public/voxl-docker-support) |
| [VOXL IMU](/voxl-imu/)                     | **voxl_imu_0.0.4_8x96.ipk**        |                                                              |
| [VOXL ROS Nodes](/ros-nodes-and-usage/)    | voxl-nodes_0.0.6_8x96.ipk          | [Source](https://gitlab.com/voxl-public/voxl-nodes)          |
| [VOXL RTSP](https://docs.modalai.com/voxl-rtsp/)                                | voxl-rtsp_1.0.0_8x96.ipk           | [Source](https://gitlab.com/voxl-public/voxl-rtsp)           |
| [VOXL Utils](/voxl-utils/)                 | **voxl-utils_0.4.6.ipk**           | [Source](https://gitlab.com/voxl-public/voxl-utils)          |
| [VOXL Vision PX4](/voxl-vision-px4/)       | **voxl-vision-px4_0.4.1_8x96.ipk** | [Source](https://gitlab.com/voxl-public/voxl-vision-px4)     |

### V0.0.3

The package list:

| Name                                       | Package                        | Notes                                                        |
|:-------------------------------------------|:-------------------------------|:-------------------------------------------------------------|
|                                            | cellular_tools_0.1.5_8x96.ipk  |                                                              |
| [Docker](/docker-on-voxl/)                 | docker_1.9.0+git...aarch64.ipk |                                                              |
| [IMU app](https://docs.modalai.com/snap-imu/)                                    | imu_app_0.0.3_8x96.ipk         | Soon to be replaced by voxl-imu                              |
| [RC Math Library](/librc-math/)            | librc_math_1.0.1.ipk           | [Source](https://gitlab.com/voxl-public/librc_math)          |
| [VOXL IO Library](/libvoxl-io/)            | libvoxl_io_0.0.6.ipk           | [Source](https://gitlab.com/voxl-public/libvoxl_io)          |
| [ModalAI Vision Lib](/modalai-vision-lib/) | modalai-vl_0.0.8_8x96.ipk      |                                                              |
| [VOXL Camera Manager](/voxl-cam-manager/)  | voxl-cam-manager_0.2.1.ipk     | [Source](https://gitlab.com/voxl-public/voxl-cam-manager)    |
| [VOXL Docker Support](/docker-on-voxl/)    | voxl-docker-support_1.1.0.ipk  | [Source](https://gitlab.com/voxl-public/voxl-docker-support) |
| [VOXL IMU](/voxl-imu/)                     | voxl_imu_0.0.3_8x96.ipk        |                                                              |
| [VOXL ROS Nodes](/ros-nodes-and-usage/)    | voxl-nodes_0.0.6_8x96.ipk      | [Source](https://gitlab.com/voxl-public/voxl-nodes)          |
| [VOXL RTSP](https://docs.modalai.com/voxl-rtsp/)                                | voxl-rtsp_1.0.0_8x96.ipk       | [Source](https://gitlab.com/voxl-public/voxl-rtsp)           |
| [VOXL Utils](/voxl-utils/)                 | voxl-utils_0.4.4.ipk           | [Source](https://gitlab.com/voxl-public/voxl-utils)          |
| [VOXL Vision PX4](/voxl-vision-px4/)       | voxl-vision-px4_0.4.0_8x96.ipk | [Source](https://gitlab.com/voxl-public/voxl-vision-px4)     |

### Checking Version

Run the following command:

```bash
voxl-version
```

or you can check the version of the VOXL Software Bundle by looking at the version file:

```bash
cat /etc/modalai/voxl-software-bundle-version.txt
0.0.5
```
