---
layout: default
title: Home
nav_order: 1
description: "VOXL Documentation"
permalink: /
youtubeId: v9hG6LZdmHM
---

# ModalAI Technical Documentation

Welcome to ModalAI's technical documentation.  ModalAI's goal is to have a complete, fully-documented, development environment for using its products for autonomous aerial and ground vehicles. If you see something missing, please contribute [here](https://gitlab.com/voxl-public/documentation), or ask for help [here](https://support.modalai.com)!

# ModalAI VOXL

VOXL is ModalAI's autonomous computing platform built around the Snapdragon 821. The VOXL architecture combines a single board computer with a depth camera, flight controller and cellular modem to create fully autonomous, connected drones and robots!

{% include youtubePlayer.html id=page.youtubeId %}

## VOXL and Associated Products
### VOXL m500 Development Drone
Get up and running with VOXL autonomous flight quickly with the [VOXL m500](https://modalai.myshopify.com/products/voxl-m500-r1/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink) bind and fly development drone. Our m500 drone is built from the popular s500 hobby kit and includes a VOXL Flight Deck configured and tested for GPS-based autonomous flight and indoor GPS-denied navigation. Obstacle avoidance stereo cameras come mounted and calibrated. This is a powerful Qualcomm Snapdragon reference drone, ready for advanced autonomy development.

### PX4 Companion Computer
[VOXL](https://www.modalai.com/voxl/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink) ([Datasheet](https://docs.modalai.com/voxl-datasheet/)) is a light-weight, but powerful companion computer for PX4. It is driven by a Qualcomm Snapdragon 821, runs Linux, has tons of I/O and pre-certified WiFi. 

Watch the [demo video](http://bit.ly/modalvid5) and buy [here](https://www.modalai.com/voxl/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink)!

### Navigation and Obstacle Avoidance

The [VOXL Flight Deck](https://www.modalai.com/flight-deck/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink) is a fully assembled and flight ready drone development platform hosting a ModalAI VOXL drone and robotics companion computer paired with the ModalAI [Flight Core](https://www.modalai.com/flight-core/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink) PX4 flight controller. 

The Flight Deck comes pre-configured with Tracking, Stereo and Hi-Res optical sensors, providing the easiest way to add US built GPS-Denied Navigation and Obstacle Avoidance to a vehicle. The Flight Deck weighs only 91g. 

Watch the [demo video](http://bit.ly/modalvid3) and buy [here](https://www.modalai.com/flight-deck/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink)!

### Cellular Communications

The [VOXL Cellular Add-on](https://www.modalai.com/voxl-lte/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink) ([Datasheet](https://docs.modalai.com/lte-modem-and-usb-add-on-datasheet/)) is an add-on that plugs in to the VOXL's board to board connector. It enables LTE capability for the following North American cellular carriers: Verizon, AT&T, and T-Mobile. 

Watch the [demo video](http://bit.ly/modalvid2) and buy [here](https://www.modalai.com/voxl-lte/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink)!

### Flight Controller

The ModalAI [Flight Core](https://www.modalai.com/flight-core/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink) ([Datasheet](https://docs.modalai.com/flight-core-datasheet/)) runs the PX4 flight controller, and is made in the USA. The Flight Core can be paired with [VOXL](https://www.modalai.com/voxl) for obstacle avoidance and GPS-denied navigation, or used independently as a standalone flight controller.

## Why VOXL?

The VOXL technology builds on the Qualcomm® Flight Pro™ architecture by adding comprehensive software development support with a build-able Linux kernel, cross-compile SDKs, LTE, time of flight cameras, [Docker images](https://gitlab.com/voxl-public/voxl-docker) for development and more.

## Get Started with VOXL

Purchase [here](https://www.modalai.com/voxl/?utm_source=modalai_docs&utm_medium=link&utm_campaign=weblink), then follow the [VOXL Quickstart Guide](/voxl-quickstarts/) and the rest of our user manuals in the bar on the left!

![VOXL](images/index-image.png)